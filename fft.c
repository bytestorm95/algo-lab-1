#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct _complex{
	double real;
	double img;
} Complex;

Complex newComplex(double real,double img);
Complex multiply(Complex a,Complex b);
Complex add(Complex a,Complex b);
Complex sub(Complex a,Complex b);

Complex multiply_r(Complex a,double d);

void echoComplex(Complex c);
void echoComplexArray(Complex* complexArr,int count);
Complex* FFT(double* x,int N, int arrSize, int zeroLoc, int gap);
Complex* IFFT(Complex* fft,int N, int arrSize, int zeroLoc, int gap);
Complex* modulate(Complex* fft,int sampleCount,int* division);
double absolute(Complex a);


//FILE HANDLERS FOR SAVING PLOT DATA
int numCommand = 5;
FILE* gnuplotpipe;
char* commandForGnuplot[] = {
	"set style fill pattern 2 bo 1",
	"set title \"Sin Function\"",
	"plot 'input1.txt' using 1:2 title 'plot1' with lines,\
	 'input1.txt' using 1:3 title 'plot2' with lines",
	"set xr [-5:5]",
	"set yr [-5:5]",
	"plot 'input2.txt' with lines"
};


int main()
{
	//OPEN THE FILE TO STORE PLOT DATA
	FILE* fileInput1 = fopen("input1.txt","w");
    FILE* fileInput2 = fopen("input2.txt","w");
	gnuplotpipe = popen("gnuplot -persistent","w");

	Complex a = newComplex(2,3);
	Complex b = newComplex(2,3);
	int i;
    
	// echoComplex(add(a,b));
	// echoComplex(sub(a,b));
	// echoComplex(multiply(a,b));
	printf("FFT OUTPUT: \n");
	// int N=1024;
	// scanf("%d",&N);
	// double x[N];
	// for (i = 0; i < N; ++i)
	// {
	// 	x[i] = i;
	// }
	// Complex* fft = FFT(x,N,N,0,1);
	// echoComplexArray(fft,N);

	int sampleCount;
	sampleCount = 128;
    
    double amp = 10;
    double* signal1 = (double*)malloc(sampleCount*sizeof(double));
    double* signal2 = (double*)malloc(sampleCount*sizeof(double));
    double f1 = 350;
    double f2 = 650;
    double Fs = 5000;
    for(i=0;i<sampleCount;i++)
    {
        signal1[i] = sin(2*M_PI*f1*i/Fs);
        signal2[i] = cos(2*M_PI*f2*i/Fs);
        // fprintf(fileInput1,"%d %f\n",i,signal1[i]);
        // fprintf(fileInput2,"%d %f\n",i,signal2[i]);
    }
    Complex* fft = FFT(signal1,sampleCount,sampleCount,0,1);
    Complex* fft2 = FFT(signal2,sampleCount,sampleCount,0,1);

    int division1[6] = {8,1,119,119,1,8}; 
    int division2[6] = {47,1,80,80,1,47}; 

    Complex* fft_mod_signal1 = modulate(fft,sampleCount,division1);
    Complex* fft_mod_signal2 = modulate(fft2,sampleCount,division2);
    // Complex* ifft = IFFT(fft,sampleCount,sampleCount,0,1);
    int k = 256*sampleCount;
    //MODULATED SIGNALS IN TIME DOMAIN
    Complex* mod_signal1 = IFFT(fft_mod_signal1,k,k,0,1);
    Complex* mod_signal2 = IFFT(fft_mod_signal2,k,k,0,1);
    //ADD THE MODULATED SIGNAL IN TIME DOMAIN
    //TO BE TRANSMITTED(FDM)
    double* fdm = (double*)malloc(k*sizeof(double));
    for (i = 0; i < k; ++i)
    {
    	fdm[i] = mod_signal1[i].real+mod_signal2[i].real;
    }
    //FIND FFT OF TRANSMITTED SIGNAL
    Complex* fft_fdm = FFT(fdm,k,k,0,1);
    Complex* retrieve_1 = fft_fdm + division1[0]*sampleCount;
    Complex* retrieve_2 = fft_fdm + division2[0]*sampleCount;
    // for (i = 0; i < sampleCount; ++i)
    // {
    // 	retrieve_1[sampleCount - i] = fft_fdm[division1[0]*sampleCount+i];
    // 	retrieve_2[sampleCount - i] = fft_fdm[division2[0]*sampleCount+i];
    // }
    Complex* demod_1 = IFFT(retrieve_1,sampleCount,sampleCount,0,1);
    Complex* demod_2 = IFFT(retrieve_2,sampleCount,sampleCount,0,1);

    printf("What do you want to plot?\n");
    printf("1 : Original Signals\n");
    printf("2 : FFT of original signals\n");
    printf("3 : Modulated Time Domain signal 1 \n");
    printf("4 : Modulated Time Domain signal 2\n");
    printf("5 : Transmitted Time Domain signal - FDM\n");
    printf("6 : FFT of FDM\n");
    printf("7 : Retrieve/Demodulate Signals\n");

    int key = 0;
    scanf("%d",&key);
    double freqPerUnit,timeUnit;
    switch(key)
    {
    	case 1:
    		timeUnit = 1/Fs;
    		for(i=0;i<sampleCount;i++)
		    {
		        fprintf(fileInput1,"%f %f %f\n",i*timeUnit,signal1[i],signal2[i]);
		    }
    	break;
    	case 2:
    		freqPerUnit = Fs/sampleCount;
    		for(i=0;i<sampleCount;i++)
		    {
		    	
		        fprintf(fileInput1,"%f %f %f\n",i*freqPerUnit,absolute(fft[i]),absolute(fft2[i]));
		    }
    	break;
    	case 3:
    		timeUnit = 1/(Fs*k/sampleCount);
    		for(i=0;i<k;i++)
		    {
		        fprintf(fileInput1,"%f %f\n",i*timeUnit,mod_signal1[i].real);
		    }
    	break;
    	case 4:
    		timeUnit = 1/(Fs*k/sampleCount);
    		for(i=0;i<k;i++)
		    {
		        fprintf(fileInput1,"%f %f\n",i*timeUnit,mod_signal2[i].real);
		    }
    	break;
    	case 5:
    		timeUnit = 1/(Fs*k/sampleCount);
    		for(i=0;i<k;i++)
		    {
		        fprintf(fileInput1,"%f %f\n",i*timeUnit,fdm[i]);
		    }
    	break;
    	case 6:
    		freqPerUnit = Fs/sampleCount;
    		for(i=0;i<k;i++)
		    {
		    	

		        fprintf(fileInput1,"%f %f\n",i*freqPerUnit,absolute(fft_fdm[i]));
		        
		    }
    	break;
    	case 7:
    		timeUnit = 1/Fs;
    		for(i=0;i<sampleCount;i++)
		    {
		        fprintf(fileInput1,"%f %f %f\n",i*timeUnit,demod_1[i].real,demod_2[i].real);
		    }
    	break;
    }   

    //CODE TO INVOKE THE GNUPLOT COMMANDS
    for (i = 0; i < numCommand; ++i)
	{
		fprintf(gnuplotpipe, "%s \n", commandForGnuplot[i]);
	}
	fclose(fileInput1);
    fclose(fileInput2);
}
void echoComplex(Complex c)
{

	if(c.img>=0)
	{
		printf("%.2f + %.2fi\n", c.real, c.img);
	}
	else
	{
		printf("%.2f - %.2fi\n", c.real, -c.img);
	}
}
void echoComplexArray(Complex* complexArr,int count)
{
	int i;
	for (i = 0; i < count; ++i)
	{
		echoComplex(complexArr[i]);
	}
}
Complex newComplex(double real,double img)
{
	Complex c;
	c.real = real;
	c.img = img;
	return c;
}
//ARITHMETIC OPERATIONS COMPLEX WITH COMPLEX
Complex multiply(Complex a,Complex b)
{
	double k1 = b.real*(a.real + a.img);
	double k2 = a.real*(b.img - b.real);
	double k3 = a.img*(b.img + b.real);

	Complex c = newComplex(k1 - k3, k1 + k2);
	return c;
}

Complex add(Complex a,Complex b)
{
	Complex c = newComplex(a.real + b.real,a.img+b.img);
	return c;
}
Complex sub(Complex a,Complex b)
{
	Complex c = newComplex(a.real - b.real,a.img - b.img);
	return c;
}
//ARITHMETIC OPERATIONS COMPLEX WITH DOUBLE
Complex multiply_r(Complex a,double d)
{
	Complex c = newComplex(a.real*d, a.img*d);
	return c;
}
double absolute(Complex a)
{
	return sqrt(a.real*a.real + a.img*a.img);
}

//ASSUMPTIONS
//WHEN CALLING THIS FUNCTION
//arrSize = N
//gap = 1
//zeroLoc = 0
Complex* FFT(double* x,int N, int arrSize, int zeroLoc, int gap)
{
	Complex* fft;
	int i;
	if(N == 2)
	{
		fft = (Complex*)malloc(N*sizeof(Complex));
		fft[0] = newComplex(x[zeroLoc] + x[zeroLoc+gap],0);
		fft[1] = newComplex(x[zeroLoc] - x[zeroLoc+gap],0);
	}
	else
	{
		fft = (Complex*)malloc(N*sizeof(Complex));

		Complex wN = newComplex(cos(2*M_PI/N),sin(-2*M_PI/N));//exp(-j2*pi/N)
		Complex w = newComplex(1,0);
		gap*=2;
		Complex* X_even = FFT(x,N/2,arrSize,zeroLoc,gap); //N/2 POINT DFT OF EVEN X's
		Complex* X_odd = FFT(x,N/2,arrSize,zeroLoc+(arrSize/N),gap); //N/2 POINT DFT OF ODD X's
		Complex todd;
		for (i = 0; i < N/2; ++i)
		{
			//FFT(0) IS EQUAL TO FFT(N-1) SYMMETRICAL AROUND N/2
			todd = multiply(w,X_odd[i]);
			fft[i] = add(X_even[i], todd);
			fft[i+N/2] = sub(X_even[i], todd);
			w = multiply(w,wN);
		}

		free(X_even);
		free(X_odd);
	}

	return fft;
}

Complex* IFFT(Complex* fft,int N, int arrSize, int zeroLoc, int gap)
{
	Complex* signal;
	int i;
	if(N == 2)
	{
		signal = (Complex*)malloc(N*sizeof(Complex));
		signal[0] = add(fft[zeroLoc], fft[zeroLoc+gap]);
		signal[1] = sub(fft[zeroLoc], fft[zeroLoc+gap]);
	}
	else
	{
		signal = (Complex*)malloc(N*sizeof(Complex));

		Complex wN = newComplex(cos(2*M_PI/N),sin(2*M_PI/N));//exp(-j2*pi/N)
		Complex w = newComplex(1,0);
		gap*=2;
		Complex* X_even = IFFT(fft,N/2,arrSize,zeroLoc,gap); //N/2 POINT DFT OF EVEN X's
		Complex* X_odd = IFFT(fft,N/2,arrSize,zeroLoc+(arrSize/N),gap); //N/2 POINT DFT OF ODD X's
		Complex todd;
		for (i = 0; i < N/2; ++i)
		{
			//FFT(0) IS EQUAL TO FFT(N-1) SYMMETRICAL AROUND N/2
			todd = multiply(w,X_odd[i]);
			signal[i] = multiply_r(add(X_even[i], todd),0.5);
			signal[i+N/2] = multiply_r(sub(X_even[i], todd),0.5);
			w = multiply(w,wN);
		}

		free(X_even);
		free(X_odd);
	}

	return signal;
}
Complex* modulate(Complex* fft,int sampleCount,int* division)
{
	// 8 1 119 119 1 8
	Complex* fft_mod_signal;
    int k = 256*sampleCount;//K-PT DFT OF MODULATED SIGNAL
    fft_mod_signal = (Complex*)malloc(k*sizeof(Complex));
    int m,n,i;
    int offset = 0;
    for (i = 0; i < sampleCount; ++i)
    {
    	offset = 0;
    	//8*N zeros
    	for (m = offset; m < offset+division[0]; ++m)
    	{
    		fft_mod_signal[i+sampleCount*m] = newComplex(0,0);
    	}
    	offset = m;
    	//N FFT
    	for (m = offset; m < offset+division[1]; ++m)
    	{
    		fft_mod_signal[i+sampleCount*m] = fft[sampleCount - i];
    	}
    	offset = m;
    	//238*N zeros
    	for (m = offset; m < offset+division[2]+division[3]; ++m)
    	{
    		fft_mod_signal[i+sampleCount*m] = newComplex(0,0);
    	}
    	offset = m;
    	//N FFT
    	for (m = offset; m < offset+division[4]; ++m)
    	{
    		fft_mod_signal[i+sampleCount*m] = fft[i];
    	}
    	offset = m;
    	//8*N zeros
    	for (m = offset; m < offset+division[5]; ++m)
    	{
    		fft_mod_signal[i+sampleCount*m] = newComplex(0,0);
    	}
    }
    return fft_mod_signal;
}
