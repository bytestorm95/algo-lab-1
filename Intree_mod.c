#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

//INTERSECTION TYPES


//Node Structure of BST
typedef struct _node{
	struct _node* left;
	struct _node* right;
	struct _node* p;
	int height;
	int keyLow;
	int high;
	int maxNode;
	int leftCount;
	int rightCount;
} Node;
typedef struct _listItem
{
	Node* this;
	struct _listItem* next;
} ListItem;
typedef struct _list
{
	ListItem* start;
	ListItem* end;
} List;

//Create a new BST Node
Node* newNode(int kLow,int kHigh);

int fixHeight(Node* node);
//Inserts a node into a Balanced BST(AVL-Tree)
Node* insert(Node* _tree, Node* _n); //No I/O operations
void updateCounts(Node* _n);

//Searches for a key in a tree and returns 
//a list of pointers to all the nodes with that key
void search(Node* _tree,int kLow,int kHigh,List* result); //No I/O operations
void searchIntersection(Node* _tree,int kLow,int kHigh,List* result);

Node* delete(Node* _tree,int keyLow, int high); //No I/O operations

//Check whether the node has any height imbalance or not
int getHImbalance(Node* _n);
//Rotates the node properly to fix an imbalanced tree
Node* fixImbalance(Node* _tree,Node* _n);

//Function that prints the result of In-order traversal of a tree
void ioTraverse(Node* _tree);

void pTraverse(Node* _tree);

//Rotates a node in anti-clockwise direction
Node* leftRotate(Node* n);
////Rotates a node in anti-clockwise direction
Node* rightRotate(Node* n);

//Returns an pointer to a Linked List node
ListItem* newListItem(Node* a);
//Creates a Linked List
List* newList(ListItem* start,ListItem* end);
//Inserts into a Linked List
void listInsert(List* l,ListItem* li);
//Prints out a Linked List
void listPrint(List* l);
int getRank_ofInterval(Node* _node, int rank, int flag_add_or_not);
void getInterval_byRank(Node* tree,int rank);
int getFreeIntervals(Node* tree, int lastMax, List* freeIntervals);
void fixSubtreeCount(Node *a);

int main(int argc,char *argv[])
{
	if(argc == 1)
	{
		printf("Run with one of the following options: \n");
		printf("I low high\n");
		printf("S ky\n");
		printf("D ky\n");
		printf("T\n");
	}
	int i;
	Node* tree = NULL;

	int keyL,keyH;
	printf("\n");
	printf("---------AVL TREE PROGRAM----------\n");
	for (i = 1; i < argc;)
	{
		switch(**(argv+i))
		{
			case 'I':
				i++;
				keyL = atoi(*(argv+i));
				i++;
				keyH = atoi(*(argv+i));
				i++;
				printf("Inserting %d and %d\n", keyL, keyH);
				tree = insert(tree,newNode(keyL,keyH));
			break;
			case 'S':
				i++;
				keyL = atoi(*(argv+i));
				i++;
				keyH = atoi(*(argv+i));
				printf("SEARCHING FOR %d ... %d:\n\n", keyL, keyH);
				printf("INTERVAL\n");
				List* result = newList(NULL,NULL);
				searchIntersection(tree,keyL,keyH,result);
				listPrint(result);
				i++;
			break;
			case 'D':
				i++;
				keyL = atoi(*(argv+i));
				i++;
				keyH = atoi(*(argv+i));
				printf("DELETE %d ... %d :\n", keyL, keyH);
				tree = delete(tree,keyL,keyH);
				i++;
			break;
			case 'T':
				printf("-----------------------------------\n");
				printf("--------PRINTING ALL DATA----------\n");
				printf("-----------------------------------\n");
				printf("INTERVAL\t\tNODE HEIGHT\n");
				printf("-----------------------------------\n");
				ioTraverse(tree);
				printf("-----------------------------------\n");
				i++;
			break;
			case 'P':
				printf("--------------------------------------------------\n");
				pTraverse(tree);
				i++;
			break;
			case 'R':
				i++;
				keyL = atoi(*(argv+i));
				i++;
				keyH = atoi(*(argv+i));
				i++;
				printf("Rank of Interval [%d,%d]\n", keyL, keyH);

				List* result2 = newList(NULL,NULL);
				search(tree,keyL,keyH,result2);

				ListItem* curr = result2->start;
				Node* _node;
				
				if(curr == NULL)
				{
					printf("\nNO RESULTS FOUND! \n");
					return;
				}
				else
				{
					int rank = getRank_ofInterval(curr->this,0,1);
					printf(" %d\n", rank);
				}
			break;
			case 'F':
				i++;
				keyH = atoi(*(argv+i));
				printf("Looking for rank %d... :\n", keyH);
				getInterval_byRank(tree,keyH);
				i++;
			break;
			case 'L':
				i++;
				List* freeIntervals = newList(NULL,NULL);
				printf("FREE INTERVALS\n");
				int temp=getFreeIntervals(tree,-INT_MAX,freeIntervals);
				listInsert(freeIntervals,newListItem(newNode(temp,INT_MAX)));
				listPrint(freeIntervals);
			break;
		}		
	}
	// int i;
	// for (i = 0; i < size; ++i)
	// {
	// 	int temp = rand()%100;
	// 	//printf("Inserting: %d\n", temp);
	// 	tree = insert(tree,newNode(temp));
	// 	//ioTraverse(tree);
	// 	printf("Height: %d\n", tree->height);
	// }
	// ioTraverse(tree);
	return 0;
	
}

Node* newNode(int kLow,int kHigh)
{
	Node *a = (Node*)malloc(sizeof(Node));
	a->left = a->right = a->p = NULL;
	a->keyLow = kLow;
	a->high = kHigh;
	a->maxNode = kHigh;
	a->height = 0;
	a->leftCount = 0;
	a->rightCount = 0;
	return a;
}

Node* insert(Node* _tree, Node* _n)
{
	Node* y = NULL;
	Node* x = _tree;
	while(x!=NULL)
	{
		y = x; // TRAVERSE THROUGH THE TREE UNTIL THE PROPER NODE IS FOUND
		if (x->keyLow > _n->keyLow)
		{
			x = x->left;
		}
		else if(x->keyLow < _n->keyLow)
		{
			x = x->right;
		}
		else
		{
			if(x->high > _n->high)
			{
				x = x->left;
			}
			else if(x->high < _n->high)
			{
				x = x->right;
			}
			else
			{
				printf("Insertion failed. Node exists.\n");
				return _tree;
			}
		}
	}
	_n->p = y;
	if(y == NULL)
	{
		_tree = _n; //THE TREE IS EMPTY
		//increaseHeight(_tree);
		return _tree;
	}
	//INSERT THE NODE IN THE PROPER SUBTREE

	if (y->keyLow > _n->keyLow)
	{
		y->left = _n;
	}
	else if(y->keyLow < _n->keyLow)
	{
		y->right = _n;
	}
	else
	{
		if(y->high > _n->high)
		{
			y->left = _n;
		}
		else
		{
			y->right = _n;
		}
	}
	updateCounts(_n);
	if(y->height == 0);

	int imBalanceFlag = 0;
	int canChangeHeight = 1;
	int tree_changed = 0;
	_n = y;
	Node *gp,*p,*c,*result;
	while(_n!=NULL)
	{

		result = _n;
		imBalanceFlag = getHImbalance(_n);
		//printf("imBalanceFlag: %d, Key: %d\n", imBalanceFlag,_n->key);		
		switch(imBalanceFlag)
		{
			case 0: //BALANCED
				//printf(" BALANCED\n");
			break;
			case -2: //RIGHT-LEFT
				//printf("Imbalance RIGHT-LEFT at %d\n", _n->key);
				//Rotate 1
				_n->right = rightRotate(_n->right);
				//Rotate 2
				_n = leftRotate(_n);
			break;
			case -1: //RIGHT-RIGHT
				//printf("Imbalance RIGHT-RIGHT at %d\n", _n->key);
				//Rotate 2
				_n = leftRotate(_n);
			break;
			case 1: //LEFT-LEFT
				//printf("Imbalance LEFT-LEFT at %d\n", _n->key);
				//Rotate 1
				_n = rightRotate(_n);
			break;
			case 2: //LEFT-RIGHT
				//printf("Imbalance LEFT-RIGHT at %d\n", _n->key);
				//Rotate 1
				_n->left = leftRotate(_n->left);
				//Rotate 2
				_n = rightRotate(_n);
			break;
		}
		if(_n->p == NULL)
		{
			_tree = _n;
			tree_changed = 1;
		}
		if(imBalanceFlag != 0)
		{
			canChangeHeight = 0;
			//printf(" At node %d Returning...\n",_n->key);
			//ROTATE
		}
		else if(canChangeHeight == 1)
		{
			int h;
			//printf("Changing Height of %d\n", _n->key);
			if(_n->left != NULL)
				h = _n->left->height;
			else
				h = -1;
			if(_n->right == NULL)
				_n->height = h+1;
			else if(_n->right != NULL)
				_n->height = h>_n->right->height?h+1:_n->right->height+1;
		}
		if(tree_changed == 1)
			break;
		_n = _n->p;
	}
	//printf("At node %d\n", _tree->key);
	return _tree;
} 
void updateCounts(Node* _n)
{
	//_n IS THE NODE JUST INSERTED OR PARENT OF THE NODE JUST REMOVED
	// if(_n->p==NULL)
	// 	return;
	// if(_n == _n->p->left)
	// {
	// 	_n->p->leftCount++;
	// }
	// else
	// {
	// 	_n->p->rightCount++;
	// }
	if(_n == NULL)
		return;
	fixSubtreeCount(_n);
	updateCounts(_n->p);
}
void search(Node* _tree,int kLow,int kHigh,List* result)
{
	if(result == NULL || _tree == NULL)
		return;
	int size = 0;
	if(_tree->keyLow == kLow)
	{
		if(_tree->high == kHigh)
		{
			listInsert(result,newListItem(_tree));
			return;
		}
		else if(_tree->high > kHigh)
		{
			search(_tree->left,kLow,kHigh,result);
		}
		else
		{
			search(_tree->right,kLow,kHigh,result);
		}
	}
	else
	{
		if(kLow < _tree->keyLow)
			search(_tree->left,kLow,kHigh,result);
		else if(kLow > _tree->keyLow)
			search(_tree->right,kLow,kHigh,result);
	}			
}
void searchIntersection(Node* _tree,int kLow,int kHigh,List* result)
{
	if(result == NULL || _tree == NULL)
		return;
	if(_tree->maxNode < kLow)
		return;
	int size = 0;
	// printf("Searching...\n");
	if(_tree->keyLow == kLow)
	{
		listInsert(result,newListItem(_tree));
		searchIntersection(_tree->left,kLow,kHigh,result);
		searchIntersection(_tree->right,kLow,kHigh,result);	
	}
	else
	{
		if(kLow < _tree->keyLow)
		{
			if(intersect(_tree,kLow,kHigh))
			{
				listInsert(result,newListItem(_tree));
				searchIntersection(_tree->right,kLow,kHigh,result);
			}
			searchIntersection(_tree->left,kLow,kHigh,result);
		}			
		else if(kLow > _tree->keyLow)
		{
			if(intersect(_tree,kLow,kHigh))
			{
				listInsert(result,newListItem(_tree));
				searchIntersection(_tree->left,kLow,kHigh,result);
			}
			searchIntersection(_tree->right,kLow,kHigh,result);
		}			
	}			
}

int intersect(Node* _n,int kLow,int kHigh)
{
	if(_n->keyLow > kLow && _n->keyLow >= kHigh )
	{
		//Interval is at the left of node.
		return 0;
	}
	else if(_n->high <= kLow && _n->high < kHigh)
	{
		//Interval is at the right of node.
		return 0;
	}
	return 1;
}
ListItem* newListItem(Node* a)
{
	ListItem* li= (ListItem*)malloc(sizeof(ListItem));
	li->this = a;
	li->next = NULL;
	return li;
}
List* newList(ListItem* start,ListItem* end)
{
	List* a = (List*)malloc(sizeof(List));
	a->start = start;
	a->end = end;
	return a;
}
void listInsert(List* l,ListItem* li)
{
	if(l->start == NULL)
	{
		l->end = l->start = li;
	}
	else
	{
		l->end->next = li;
		l->end = li;
	}
}
void listPrint(List* l)
{
	ListItem* curr = l->start;
	Node* _node;
	
	if(curr == NULL)
	{
		printf("\nNO RESULTS FOUND! \n");
		return;
	}
	else
	{
		printf("--------------\n");
	}
	while(curr!=NULL)
	{
		_node = curr->this;
		if( _node->keyLow != -INT_MAX && _node->high != INT_MAX)
		{
			printf("%d ... %d\n\n", _node->keyLow,_node->high);
		}
		else if( _node->keyLow == -INT_MAX)
		{
			printf("-inf ... %d\n\n",_node->high);
		}
		else
		{
			printf("%d ... inf\n\n", _node->keyLow);
		}
		curr = curr->next;
	}
}
Node* delete(Node* _tree,int keyLow, int high)
{
	List* result = newList(NULL,NULL);
	search(_tree,keyLow,high,result);
	ListItem* curr = result->start;
	Node* toDelete;
	if(curr == NULL)
	{
		printf("Key doesn't exist.\n");
		return _tree;
	}
	Node *swap,*p,*temp,*c,*parent;

	while(curr!=NULL)
	{
		toDelete = curr->this;
		//free(curr);
		result->start = result->start->next;
		printf("\n");
		switch(countChild(toDelete))
		{
			case 0: // NO CHILD -- A LEAF
				//JUST REMOVE THE NODE AND UPDATE IT'S PARENT
				if(toDelete->p != NULL)
				{
					if(toDelete == toDelete->p->right)
					{
						// printf("On the right of %d\n", toDelete->p->key);
						toDelete->p->right = NULL;
					}
					else
					{
						// printf("On the left of %d\n", toDelete->p->key);
						toDelete->p->left = NULL;
					}
					parent = toDelete->p;
					fixHeight(parent);
					updateCounts(parent);
					_tree = fixImbalance(_tree,parent);
				}
				else
					_tree = NULL;
				//free(toDelete->data);
				free(toDelete);
				//free(curr);
			break;
			case 2: //HAVE BOTH CHILDREN
			case -1: // HAVE ONLY RIGHT CHILD
				swap = toDelete->right;
				while(swap->left!=NULL)
				{
					swap = swap->left;
				}
				// printf("Swapper: %d\n", swap->key);
				if(toDelete->p != NULL)
				{				
					if(toDelete == toDelete->p->left)
					{
						toDelete->p->left = swap;
					}
					else
					{
						toDelete->p->right = swap;
					}
				}
				else
				{
					// printf("Deleting Root node\n");
					_tree = swap;
				}
				swap->left = toDelete->left;
				
				if(toDelete->left!=NULL)
				{
					swap->left->p = swap;
				}

				

				if(swap->p != toDelete)
				{
					//SWAP ISN'T IMMEDIATE RIGHT NODE OF TODELETE

					//ATTACH RIGHT NODE OF SWAP TO THE PARENT OF SWAP
					swap->p->left = swap->right;
					if(swap->right != NULL)
					{
						swap->right->p = swap->p;
						fixHeight(swap->p);
					}

					//MODIFY RIGHT OF SWAP
					swap->right = toDelete->right;
					if(toDelete->right!=NULL)
					{
						swap->right->p = swap;
					}
					
					Node* oldP = swap->p;
										
					swap->p = toDelete->p;
					temp = oldP;
					while(temp!=NULL)
					{
						fixHeight(temp);
						temp = temp->p;
					}
					updateCounts(oldP);
					_tree = fixImbalance(_tree,oldP);			
					free(toDelete);
				}
				else
				{
					//SWAP IS IMMEDIATE RIGHT NODE OF TODELETE
					swap->p = toDelete->p;
					
					temp = swap;
					while(temp!=NULL)
					{
						fixHeight(temp);
						temp = temp->p;
					}
					updateCounts(swap);
					_tree = fixImbalance(_tree,swap);			
					free(toDelete);
				}


			break;
			case 1: // HAVE ONLY LEFT CHILD
				// TODELTE'S LEFT IS A LEAF CHILD
				swap = toDelete->left;
				if(toDelete->p != NULL)
				{				
					if(toDelete == toDelete->p->left)
					{
						toDelete->p->left = swap;
					}
					else
					{
						toDelete->p->right = swap;
					}
				}
				else
				{
					_tree = swap;
				}
				swap->p = toDelete->p;
				swap->height = 0;
				temp = swap;
				while(temp!=NULL)
				{
					fixHeight(temp);
					temp = temp->p;
				}
				updateCounts(swap);
				_tree = fixImbalance(_tree,swap->p);
				free(toDelete);
			break;
		}
		curr = result->start;
	}
	return _tree;

}
int countChild(Node* _tree)
{
	if(_tree->left == NULL && _tree->right == NULL)
		return 0; // NO CHILD -- A LEAF
	if(_tree->right == NULL)
		return 1; // HAVE ONLY LEFT CHILD
	if(_tree->left == NULL)
		return -1; // HAVE ONLY RIGHT CHILD
	return 2; // HAVE BOTH CHILD
}
int fixHeight(Node* node)
{
	if(node==NULL)
		return;
	int old = node->height;
	// printf("Fixing Height of %d\n", node->key);
	// printf("Old Height: %d, ", node->height);
	if(node->left == NULL && node->right == NULL)
		node->height=0; // NO CHILD -- A LEAF
	else if(node->right == NULL)
		node->height = node->left->height+1; // HAVE ONLY LEFT CHILD
	else if(node->left == NULL)
		node->height = node->right->height+1; // HAVE ONLY RIGHT CHILD
	else
		node->height = node->right->height > node->left->height?node->right->height+1:node->left->height+1; // HAVE BOTH CHILD
	// printf("New Height: %d\n", node->height);
	if(old != node->height)
		return 1; //HEIGHT HAS BEEN CHANGED(FIXED)
	return 0; //HEIGHT HASN'T BEEN CHANGED(NO FIX)
}

int getHImbalance(Node* _n)
{
	if(_n == NULL)
		return 0;
	int hDiff = 0;
	Node* parent;
	if(_n->left!= NULL && _n->right!= NULL)
	{
		hDiff = _n->left->height - _n->right->height;
		switch(hDiff)
		{
			case 0: //BALANCED
			case 1: //BALANCED
			case -1: //BALANCED
				hDiff = 0;
			break;
			case 2: //LEFT-LEFT(1) OR LEFT-RIGHT(2) CASE
				parent = _n->left;
				if(parent->left == NULL)
				{
					hDiff = 2;
				}
				else if(parent->right==NULL)
				{
					hDiff = 1;
				}
				else
				{
					hDiff = parent->left->height>parent->right->height?1:2;	
				}		
			break;
			case -2: //RIGTH-LEFT(-2) OR RIGHT-RIGHT(-1) CASE
				parent = _n->right;
				if(parent->right == NULL)
				{
					hDiff = -2;
				}
				else if(parent->left==NULL)
				{
					hDiff = -1;
				}
				else
				{
					hDiff = parent->left->height>parent->right->height?-2:-1;	
				}		
			break;
		}
	}
	else if(_n->left != NULL)
	{
		hDiff = _n->left->height + 1; //RIGHT BRANCH TO THE PARENT IS EMPTY
		if(hDiff>=2)
		{
			parent = _n->left;
			hDiff = parent->left!=NULL?1:2;
		}
		else
			hDiff = 0;
	}
	else if(_n->right != NULL)	
	{
		hDiff = _n->right->height + 1;
		if(hDiff>=2)
		{
			parent = _n->right;
			hDiff = parent->right!=NULL?-1:-2;
		}
		else
			hDiff = 0;
	}
	return hDiff;
}

Node* fixImbalance(Node* _tree,Node* _n)
{
	if(_tree == NULL || _n==NULL)
		return;
	//printf("fix balance Called: %d\n", _n->key);
	int imBalanceFlag = 0;
	int canChangeHeight = 1;
	int tree_changed = 0;
	Node *gp,*p,*c,*result;
	while(_n!=NULL)
	{
		result = _n;
		imBalanceFlag = getHImbalance(_n);
		//printf("imBalanceFlag: %d, Key: %d\n", imBalanceFlag,_n->key);		
		switch(imBalanceFlag)
		{
			case 0: //BALANCED
				//printf(" BALANCED\n");
			break;
			case -2: //RIGHT-LEFT
				// printf("Imbalance RIGHT-LEFT at %d\n", _n->key);
				//Rotate 1
				_n->right = rightRotate(_n->right);
				//Rotate 2
				_n = leftRotate(_n);
			break;
			case -1: //RIGHT-RIGHT
				// printf("Imbalance RIGHT-RIGHT at %d\n", _n->key);
				//Rotate 2
				_n = leftRotate(_n);
			break;
			case 1: //LEFT-LEFT
				// printf("Imbalance LEFT-LEFT at %d\n", _n->key);
				//Rotate 1
				_n = rightRotate(_n);
			break;
			case 2: //LEFT-RIGHT
				// printf("Imbalance LEFT-RIGHT at %d\n", _n->key);
				//Rotate 1
				_n->left = leftRotate(_n->left);
				//Rotate 2
				_n = rightRotate(_n);
			break;
		}
		if(_n->p == NULL)
		{
			_tree = _n;
			tree_changed = 1;
		}
		_n = _n->p;
	}
	//printf("At node %d\n", _tree->key);
	return _tree;
}
void ioTraverse(Node* _tree)
{
	if(_tree != NULL)
	{

		ioTraverse(_tree->left);
		printf("%d ... %d\t\t%d,(L)%d,(R)%d\n\n", _tree->keyLow,_tree->high ,_tree->height+1, _tree->leftCount, _tree->rightCount);
		
		ioTraverse(_tree->right);
	}
}
void pTraverse(Node* _tree)
{
	if(_tree != NULL)
	{
		printf("Root: %d ... %d\t\t%d,(L)%d,(R)%d\n\n", _tree->keyLow,_tree->high ,_tree->height+1, _tree->leftCount, _tree->rightCount);
		printf("Left: \n");
		pTraverse(_tree->left);
		printf("Right: \n");
		pTraverse(_tree->right);
	}
}

Node* rightRotate(Node* n)
{
	if(n == NULL)
		return n;
	Node* c = n->left;
	c->p = n->p;
	if(n->p != NULL)
	{
		if(n->p->left == n)
			n->p->left = c;
		else
			n->p->right = c;
	}
	n->left = c->right;
	c->right = n;
	
	if(n->left!=NULL)
		n->left->p = n;
	n->p = c;
	fixHeight(n);
	fixHeight(c);
	fixSubtreeCount(n);
	fixSubtreeCount(c);
	return c;
}
Node* leftRotate(Node* n)
{
	if(n==NULL)
		return n;
	Node* c = n->right;
	c->p = n->p;
	if(n->p != NULL)
	{
		if(n->p->left == n)
			n->p->left = c;
		else
			n->p->right = c;
	}
	n->right = c->left;
	c->left = n;
	
	if(n->right!=NULL)
		n->right->p = n;
	n->p = c;
	fixHeight(n);
	fixHeight(c);
	fixSubtreeCount(n);
	fixSubtreeCount(c);
	return c;
}
int getRank_ofInterval(Node* _node, int rank, int flag_add_or_not)
{
	rank += (_node->leftCount + 1)*flag_add_or_not;
	if(_node->p != NULL)
	{
		if(_node->p->right == _node)
		{
			rank = getRank_ofInterval(_node->p, rank, 1);
		}
		else
		{
			rank = getRank_ofInterval(_node->p, rank, 0);
		}
	}

	return rank;
}
void getInterval_byRank(Node* tree,int rank)
{
	int temp = 0;
	if(rank > tree->leftCount + tree->rightCount + 1)
	{
		printf("Out of range...\n");
		return;
	}
	Node* scanner = tree;
	temp = scanner->leftCount + 1;
	
	while(scanner != NULL)
	{
		if(rank == temp)
		{
			printf("  Interval [%d ... %d] \n", scanner->keyLow,scanner->high );
			return;
		}
		if(rank > temp)
		{			
			scanner = scanner->right;	
			temp += scanner->leftCount + 1;
		}
		else
		{
			scanner = scanner->left;
			temp -= scanner->rightCount + 1;
		}
	}
}
int getFreeIntervals(Node* tree, int lastMax, List* freeIntervals)
{
	// int size = tree->leftCount + tree->rightCount + 1;
	// int count = 1;
	// while(count<=size)
	// {
	// 	Node* now = getInterval_byRank(tree,count);
		
	// } 

	if(tree != NULL)
	{
		lastMax = getFreeIntervals(tree->left,lastMax,freeIntervals);
		if(tree->keyLow > lastMax)
		{
			listInsert(freeIntervals,newListItem(newNode(lastMax,tree->keyLow)));
			lastMax = tree->high;
		}			
		else
		{
			if(tree->high > lastMax)
				lastMax = tree->high;
		}
		lastMax = getFreeIntervals(tree->right,lastMax,freeIntervals);
	}
	return lastMax;
}
void fixSubtreeCount(Node *a)
{
	int maxNode = a->high;
	Node* left = a->left;
	Node* right = a->right;
	if(left != NULL)
	{
		a->leftCount = left->leftCount + left->rightCount +1;
		if(maxNode < left->maxNode)
		{
			maxNode = left->maxNode;
		}
	}
	else
	{
		a->leftCount = 0;
	}
	if(right != NULL)
	{
		a->rightCount = right->leftCount + right->rightCount+1;
		if(maxNode < right->maxNode)
		{
			maxNode = right->maxNode;
		}
	}
	else
	{
		a->rightCount = 0;
	}
	a->maxNode = maxNode;
}

//CHECKPOST : 24/02/2015