#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <limits.h>

/* The following is the size of a buffer to contain any error messages
   encountered when the regular expression is compiled. */

#define MAX_ERROR_MSG 0x1000

typedef char* string;
typedef struct _line
{
	string* words;
	size_t word_count;
	size_t size;
} Line;
typedef struct _page{
	Line* lines;
	size_t line_count;
	size_t size;
} Page;
static int compile_regex (regex_t * r, const char * regex_text);
static int match_regex (regex_t * r, const char * to_match,Page* page,size_t line_index);
Page* newPage();
Line newLine();
void extendLine(Line* line);
void extendPage(Page* page);
void insertWord(Line* line,size_t index,string word);
void printLine(Line* line);
void printPage(Page* page);
void processFile(FILE* file,Page* page);
int wordDistance(char* s,char *t);
int findMin(int* arr,int size,short* locate);
string getEqString(Line* line);
int pageDistance2(Page* s,Page *t);

int main(int argc,char* argv[])
{	
	if(argc<3)
	{
		printf("The distance bw '%s' and '%s' is %d\n","kitten","sitting",wordDistance("kitten","sitting") );
		printf("The distance bw '%s' and '%s' is %d\n","cat","atm",wordDistance("cat","atm") );
		printf("The distance bw '%s' and '%s' is %d\n","Rhythm","them",wordDistance("thme","Rhythm") );
		printf("The distance bw '%s' and '%s' is %d\n","Sunday","Saturday",wordDistance("Sunday","Saturday") );
	}
	else
	{
		FILE* file1 = fopen(argv[1],"r");
		FILE* file2 = fopen(argv[2],"r");
		if(file1==NULL || file2==NULL)
		{
			printf("Cannot open the mentioned files.\n");
			exit(1);
		}
		Page* page1 = newPage();
		processFile(file1,page1);
		Page* page2 = newPage();
		processFile(file2,page2);

		// printPage(page1);
		// printPage(page2);
				
		printf("The page distance_2 is %d\n",pageDistance2(page1,page2));

		fclose(file1);
		fclose(file2);
	}
		
	return 0;
}


void printPage(Page* page)
{
	int i;
	printf("\nPrinting File: \n");
	for (i = 0; i < page->line_count; ++i)
	{
		
		printLine(page->lines+i);
		printf("\n");
	}
}
void printLine(Line* line)
{
	int i;
	for (i = 0; i < line->word_count; ++i)
	{
		if(line->words[i] != NULL)
		{
			printf("%s  ", line->words[i]);
		}
	}
}
string getEqString(Line* line)
{
	string str;
	str = (string)malloc(500*sizeof(char));
	int i;
	sprintf(str,"%s",line->words[0]);
	for (i = 1; i < line->word_count; ++i)
	{
		if(line->words[i] != NULL)
		{
			sprintf(str,"%s %s",str, line->words[i]);
		}
	}
	return str;
}
Page* newPage()
{
	Page* page = (Page*)malloc(sizeof(Page));
	page->size = 5;
	page->lines = (Line*)malloc(page->size*sizeof(Line));
	page->line_count = 0;
	int i;
	for (i = 0; i < page->size; ++i)
	{
		page->lines[i] = newLine();
	}
	return page;
}
Line newLine()
{
	// Line* line = (Line*)malloc(sizeof(Line));
	// line->size = 5;
	// line->words = (string*)malloc(line->size*sizeof(string));
	// return line;
	Line line;
	line.size = 5;
	line.words = (string*)malloc(line.size*sizeof(string));
	line.word_count = 0;
	int i;
	for (i = 0; i < line.size; ++i)
	{
		line.words[i] = NULL;
	}
	return line;
}
void extendLine(Line* line)
{
	int old = line->size;
	line->size = line->size*2;
	line->words = (string*)realloc(line->words,line->size*sizeof(string));
	int i;
	for (i = old; i < line->size; ++i)
	{
		line->words[i] = NULL;
	}
}
void extendPage(Page* page)
{
	int old = page->size;
	page->size = page->size*2;
	page->lines = (Line*)realloc(page->lines,page->size*sizeof(Line));
	int i;
	for (i = old; i < page->size; ++i)
	{
		page->lines[i] = newLine();
	}
}
void insertWord(Line* line,size_t index,string word)
{
	if(word == NULL)
	{
		return;
	}
    line->word_count = index+1;
	int len = strlen(word);
	char* cpy;
	cpy = (string)malloc(80*sizeof(char));
	strcpy(cpy,word);
	if(index<line->size)
	{
		line->words[index] = cpy;
	}
	else
	{
		line->size = line->size*2;
		line->words = (string*)realloc(line->words,line->size*sizeof(string));
		line->words[index] = cpy;
	}
}
/* Compile the regular expression described by "regex_text" into
   "r". */

static int compile_regex (regex_t * r, const char * regex_text)
{
    int status = regcomp (r, regex_text, REG_EXTENDED|REG_NEWLINE);
    if (status != 0) {
	char error_message[MAX_ERROR_MSG];
	regerror (status, r, error_message, MAX_ERROR_MSG);
        printf ("Regex error compiling '%s': %s\n",
                 regex_text, error_message);
        return 1;
    }
    return 0;
}

/*
  Match the string in "to_match" against the compiled regular
  expression in "r".
 */

static int match_regex (regex_t * r, const char * to_match,Page* page,size_t line_index)
{
	size_t word_index = 0;
	char word[80];
    /* "P" is a pointer into the string which points to the end of the
       previous match. */
    const char * p = to_match;
    /* "N_matches" is the maximum number of matches allowed. */
    const int n_matches = 10;
    /* "M" contains the matches found. */
    regmatch_t m[n_matches];
    int i = 0;
    while (1) {
        int nomatch = regexec (r, p, n_matches, m, 0);
        if (nomatch) {
        	if(i==0)
        	{
            	// printf ("No characters in this line.\n");
            	return 100;
        	}
            // printf ("No more matches.\n");
            // printf("%d\n", nomatch);
            return nomatch;
        }
        for (i = 1; i < n_matches; i++) {
            int start;
            int finish;
            if (m[i].rm_so == -1) {
                break;
            }
            start = m[i].rm_so + (p - to_match);
            finish = m[i].rm_eo + (p - to_match);
            if (i == 0) {
                // printf ("$& is ");
            }
            else {
                // printf ("$%d is ", i);
                sprintf(word,"%.*s",(finish-start),to_match+start);
               	insertWord(page->lines+line_index,word_index++,word);

            }
            // printf ("'%.*s' (bytes %d:%d)\n", (finish - start),
            //         to_match + start, start, finish);
            // puts(word);
        }
        p += m[0].rm_eo;
    }
    return 0;
}
void processFile(FILE* file,Page* page)
{
	string line = (string)malloc(100*sizeof(char));
	regex_t r;
	const char * regex_text;
	regex_text = "([[:graph:][:punct:]]{1,})";
	compile_regex(& r, regex_text);
	size_t page_line_num = 0;
	while(fgets(line,100,file))
	{
		// puts(line);
		// printf("%lu\n", strlen(line));
		//CHECK IF THERE IS ANY TEXT IN THE LINE


		// printf ("Trying to find '%s' in '%s'\n", regex_text, line);
		if(page_line_num < page->size)
		{
			if(match_regex(& r, line,page,page_line_num++) == 100)
			{
				page_line_num--;
			}
			else
			{
				page->line_count = page_line_num;
			}
		}
		else
		{
			extendPage(page);
			if(match_regex(& r, line,page,page_line_num++) == 100)
			{
				page_line_num--;
			}
			else
			{
				page->line_count = page_line_num;
			}
		}
	}
	regfree (& r);
}
int findMin(int* arr,int size,short* locate)
{
	int i;
	int min = INT_MAX;
	for (i = 0; i < size; ++i)
	{
		if(arr[i]<min)
		{
			min = arr[i];
			*locate = i+1;
		}
	}
	return min;
}
int wordDistance(char* s,char *t)
{
	if(s==NULL || t == NULL)
	{
		return -1;
	}
	size_t m=strlen(s);
	size_t n=strlen(t);
	short type;
	//1 - Deletion
	//2 - Insertion
	//3 - Substitution
	int **d = (int**)malloc((m+1)*sizeof(int*));
	int i,j;
	for (i = 0; i < m+1; ++i)
	{
		d[i]= (int*)malloc((n+1)*sizeof(int));
		d[i][0] = i;
	}
	for (j = 0; j < n+1; ++j)
	{
		d[0][j] = j;
	}
	for (j = 1; j < n+1; ++j)
	{
		for (i = 1; i < m+1; ++i)
		{
			if(s[i-1] == t[j-1])
			{
				d[i][j] = d[i-1][j-1];
			}
			else
			{
				int arr[3] = {d[i-1][j]+1,d[i][j-1]+1,d[i-1][j-1]+1};
				d[i][j] = findMin(arr,3,&type);
			}
		}
	}
	// for (i = 0; i < m+1; ++i)
	// {
	// 	for (j = 0; j < n+1; ++j)
	// 	{
	// 		printf("%d\t", d[i][j]);
	// 	}
	// 	printf("\n");
	// }
	return d[m][n];
}
int lineDistance(Line* s,Line* t)
{
	if(s==NULL || t == NULL)
	{
		return -1;
	}
	size_t m=s->word_count;
	size_t n=t->word_count;
	short type;
	//1 - Deletion
	//2 - Insertion
	//3 - Substitution
	int **d = (int**)malloc((m+1)*sizeof(int*));
	int **directions = (int**)malloc((m+1)*sizeof(int*));
	//1 - DELETION
	//2 - SUBSTITUTION / NO-CHANGE
	//3 - INSERTION
	int i,j;
	for (i = 0; i < m+1; ++i)
	{
		d[i]= (int*)malloc((n+1)*sizeof(int));
		directions[i]= (int*)malloc((n+1)*sizeof(int));
		d[i][0] = i;
		directions[i][0] = 1;
	}
	for (j = 0; j < n+1; ++j)
	{
		d[0][j] = j;
		directions[0][j] = 3;
	}
	for (j = 1; j < n+1; ++j)
	{
		for (i = 1; i < m+1; ++i)
		{
			if(strcmp(s->words[i-1],t->words[j-1]) == 0)
			{
				d[i][j] = d[i-1][j-1];
				directions[i][j] = 2;
			}
			else
			{
				int arr[3] = {d[i-1][j]+1,d[i][j-1]+1,d[i-1][j-1]+1};
				d[i][j] = findMin(arr,3,&type);
				switch(type)
				{
					case 1://DELETION
						directions[i][j] = 1;
					break;
					case 2://INSERTION
						directions[i][j] = 3;
					break;
					case 3://SUBSTITUTION
						directions[i][j] = 2;
					break;
				}
			}
		}
	}
	// for (i = 0; i < m+1; ++i)
	// {
	// 	for (j = 0; j < n+1; ++j)
	// 	{
	// 		printf("%d\t", d[i][j]);
	// 	}
	// 	printf("\n");
	// }
	i = m;
	j = n;
	// while(i>0 && j>0)
	// {
	// 	switch(directions[i][j])
	// 	{
	// 		case 1:
	// 			printf("Deletion: %s\n", s->words[i-1]);
	// 			i--;
	// 		break;
	// 		case 2:
	// 			if(d[i-1][j-1] < d[i][j])
	// 			{
	// 				printf("Substitution: %s by %s\n", s->words[i-1],t->words[j-1]);
	// 			}
	// 			i--;
	// 			j--;
	// 		break;
	// 		case 3:
	// 			printf("Insertion: %s\n", t->words[j-1]);
	// 			j--;
	// 		break;
	// 	}
	// }
	// while(i>0)
	// {
	// 	printf("Deletion: %s\n", s->words[i-1]);
	// 	i--;
	// }
	// while(j>0)
	// {
	// 	printf("Insertion: %s\n", t->words[j-1]);
	// 	j--;
	// }

	return d[m][n];
}
int pageDistance(Page* s,Page *t)
{
	if(s==NULL || t == NULL)
	{
		return -1;
	}
	size_t m=s->line_count;
	size_t n=t->line_count;
	short type;
	//1 - Deletion
	//2 - Insertion
	//3 - Modification
	int **d = (int**)malloc((m+1)*sizeof(int*));
	int **lineDistances = (int**)malloc((m+1)*sizeof(int*));
	int i,j;
	for (i = 0; i < m+1; ++i)
	{
		d[i]= (int*)malloc((n+1)*sizeof(int));
		lineDistances[i]= (int*)malloc((n+1)*sizeof(int));
		d[i][0] = i;
	}
	for (j = 0; j < n+1; ++j)
	{
		d[0][j] = j;
	}
	for (i = 0; i < m; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			lineDistances[i][j] = lineDistance(s->lines+i,t->lines+j);
		}
	}
	for (j = 1; j < n+1; ++j)
	{
		for (i = 1; i < m+1; ++i)
		{
			if(lineDistances[i-1][j-1] == 0)
			{
				d[i][j] = d[i-1][j-1];
			}
			else
			{
				int arr[3] = {
					d[i-1][j]+s->lines[i-1].word_count,
					d[i][j-1]+t->lines[j-1].word_count,
					d[i-1][j-1]+lineDistances[i-1][j-1]
				};
				d[i][j] = findMin(arr,3,&type);
			}
		}
	}
	// for (i = 0; i < m+1; ++i)
	// {
	// 	for (j = 0; j < n+1; ++j)
	// 	{
	// 		printf("%d\t", d[i][j]);
	// 	}
	// 	printf("\n");
	// }
	return d[m][n];
}
//COMPARE LINES AS STRINGS
int pageDistance2(Page* s,Page *t)
{
	printf("----------------------------\n");
	printf("Doing Page Comparision\n");
	printf("----------------------------\n");
	if(s==NULL || t == NULL)
	{
		return -1;
	}
	size_t m=s->line_count;
	size_t n=t->line_count;
	// printf("Line count 1 %lu\n", m);
	// printf("Line count 2 %lu\n", n);
	short type;
	//1 - Deletion
	//2 - Insertion
	//3 - Modification
	int **d = (int**)malloc((m+1)*sizeof(int*));
	int **lineDistances = (int**)malloc((m+1)*sizeof(int*));
	int **directions = (int**)malloc((m+1)*sizeof(int*));
	int i,j;
	for (i = 0; i < m+1; ++i)
	{
		d[i]= (int*)malloc((n+1)*sizeof(int));
		lineDistances[i]= (int*)malloc((n+1)*sizeof(int));
		directions[i]= (int*)malloc((n+1)*sizeof(int));
		directions[i][0] = 1;
		// if(i>0)
		// {
		// 	printf("Length_Del: [%d] %lu\n",i, strlen(getEqString(s->lines+i-1)));
		// 	puts(getEqString(s->lines+i-1));
		// }
		d[i][0] = i>0?(d[i-1][0]+strlen(getEqString(s->lines+i-1))):i;
		// d[i][0] = i;
	}
	for (j = 0; j < n+1; ++j)
	{
		// if(j>0)
		// {
		// 	printf("Length_Del: [%d] %lu\n",j, strlen(getEqString(t->lines+j-1)));
		// 	puts(getEqString(t->lines+j-1));
		// }
		d[0][j] = j>0?(d[0][j-1] + strlen(getEqString(t->lines+j-1))):j;
		directions[0][j] = 3;
	}
	for (i = 0; i < m; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			lineDistances[i][j] = wordDistance(
				getEqString(s->lines+i),
				getEqString(t->lines+j)
				);
		}
	}
	for (j = 1; j < n+1; ++j)
	{
		for (i = 1; i < m+1; ++i)
		{
			if(lineDistances[i-1][j-1] == 0)
			{
				d[i][j] = d[i-1][j-1];
				directions[i][j] = 2;
			}
			else
			{
				int arr[3] = {
					d[i-1][j]+strlen(getEqString(s->lines+i-1)),
					d[i][j-1]+strlen(getEqString(t->lines+j-1)),
					d[i-1][j-1]+lineDistances[i-1][j-1]
				};
				d[i][j] = findMin(arr,3,&type);
				switch(type)
				{
					case 1://DELETION
						directions[i][j] = 1;
					break;
					case 2://INSERTION
						directions[i][j] = 3;
					break;
					case 3://SUBSTITUTION
						directions[i][j] = 2;
					break;
				}
			}
		}
	}
	i =m;
	j=n;
	while(i>0 && j>0)
	{
		switch(directions[i][j])
		{
			case 1:
				printf("[COST: %d]Deletion: '%s'\n",d[i][j] - d[i-1][j], getEqString(s->lines+i-1));
				i--;
			break;
			case 2:
				if(d[i-1][j-1] < d[i][j])
				{
					printf("[COST: %d]Substitution: '%s' by '%s'\n",lineDistances[i-1][j-1], getEqString(s->lines+i-1),getEqString(t->lines+j-1));
				}
				i--;
				j--;
			break;
			case 3:
				printf("[COST: %d]Insertion: '%s'\n",d[i][j] - d[i][j-1], getEqString(t->lines+j-1));
				j--;
			break;
		}
	}
	while(i>0)
	{
		printf("[COST: %d]Deletion: '%s'\n",d[i][j] - d[i-1][j], getEqString(s->lines+i-1));
		i--;
	}
	while(j>0)
	{
		printf("[COST: %d]Insertion: '%s'\n",d[i][j] - d[i][j-1], getEqString(t->lines+j-1));
		j--;
	}
	// for (i = 0; i < m+1; ++i)
	// {
	// 	for (j = 0; j < n+1; ++j)
	// 	{
	// 		printf("%d\t", d[i][j]);
	// 	}
	// 	printf("\n");
	// }
	return d[m][n];
}