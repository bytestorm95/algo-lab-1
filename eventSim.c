#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#define ParentNode(i) (int)floor((i-1)/2)
#define LeftNode(i) 2*i + 1
#define RightNode(i) 2*i + 2

//COORDINATE POINT
typedef struct _point
{
	double x,y;
} Point;

//LINKED LIST DATA STRUCTURE TO STORE
//THE PATH A BALL TRAVERSED
typedef struct _path
{
	Point p;
	struct _path* next;
} Path;

//LINKED LIST DATA STRUCTURE
typedef struct _llist
{
	int* flag_p;
	struct _llist* next;
	struct _llist* end;
} FlagList;

//STRUCTURE FOR BALLS
typedef struct _physicsObject
{
	struct _physicsState* currentState;
	FlagList* allEventFlags;
	Path* path;
	double lastTS;
} PhysicsObject;

//STATE OBJECT TO STORE PHYSICAL VARIABLES
typedef struct _physicsState
{
	PhysicsObject* object;
	double x,y,vx,vy,radius;
	int color;
} PhyState;

//HEAPNODES
typedef struct _heapNode
{
	double timeStamp; //Key for heap entries
	int* isValid;	
	PhysicsObject* obj1;
	PhysicsObject* obj2;
	//WallFlags
	//1...Top
	//2...Right
	//3...Bottom
	//4...Left
	int WallFlag;
} HeapNode;

//HEAP DATA STRUCTURE
typedef struct _heap
{
	HeapNode* nodes;
	int length;
	int heapSize;
} Heap;

//RECTANGE - REPRESENTS AN WALL IN THIS CONTEXT
typedef struct _rect
{
	double left,right,top,bottom;
} Rect;

//WORLD - CONSISTS OF MORE THAN ONE PHYSICS
//OBJECTS AND AN WALL
typedef struct _world
{
	PhysicsObject** objects;
	int count;
} World;

Heap* newHeap();
World* newWorld();
HeapNode newHeapNode(double timeStamp, PhysicsObject* o1, PhysicsObject* o2);
PhyState* newState(PhysicsObject* o_p,double x,double y,double vx,double vy,int color,double radius);
void worldInsert(World* w,PhysicsObject* o);
PhysicsObject* newObject();

void setState(PhysicsObject* obj,PhyState* s);
void updatePath(PhysicsObject* obj,double x,double y);
void plotPath(PhysicsObject* obj);
void buildMinHeap(Heap* h);
void minHeapify(Heap* h, int index);
void swapNode(Heap* h,int this,int that);
void minHeapInsert(Heap* h,HeapNode node);
HeapNode heapMin(Heap* h);
HeapNode heapExtractMin(Heap* h);
void heapDecreaseKey(Heap* h, int index, HeapNode node);
void heapSort(Heap* h);
void printHeap(Heap* h);
void switchState(PhysicsObject* obj,PhyState* state);

double getCollisionTime(PhysicsObject* obj1,PhysicsObject* obj2,Heap* collisions);
double quadSolver(double a,double b,double c);
void simulateCollision(HeapNode h);
void addFlag(PhysicsObject* obj,HeapNode n);
void remFutureEvents(PhysicsObject* obj);
double computeWallCollisions(PhysicsObject* obj,Heap* collisions, Rect wall);
void printState(PhysicsObject* obj,char* message);
Point getCurrentPosition(PhysicsObject* obj,double t);


FILE* gnuplotpipe;
FILE* temp;
FILE* matFile;
FILE* logFile;
char* commandForGnuplot[] = {
	"set style line 1 lc rgb '#0000ff' lt 1 lw 2 pt 7 ps 1.5   # --- blue",
	"set style line 2 lc rgb '#ff0000' lt 1 lw 2 pt 7 ps 1.5   # --- red",
	"set style line 3 lc rgb '#00ff00' lt 1 lw 2 pt 7 ps 1.5   # --- green",
	"set style line 4 lc rgb '#ffff00' lt 1 lw 2 pt 7 ps 1.5   # --- yellow",
	"set style line 5 lc rgb '#000000' lt 1 lw 4 pt 7 ps 1.5   # --- black",	
	"plot 'data.temp' index 0 with linespoints ls 1 notitle,''          index 1 with linespoints ls 2 notitle,''          index 2 with linespoints ls 3 notitle,''          index 3 with linespoints ls 4 notitle,''          index 4 with linespoints ls 5 notitle","set title \"Path Plot\""
};
int numCommand = 7;
double curr_time =0 ;

int main()
{
	double TIME_LIM;
	printf("Please enter maximum to run the simulation: \n");
	scanf("%lf",&TIME_LIM);
	Heap* collisions = newHeap(10);
	Rect mainWall;
	mainWall.top = 7;
	mainWall.left = -7;
	mainWall.right = 7;
	mainWall.bottom = -7;
	temp = fopen("data.temp","w");
	matFile = fopen("dataPoints.mat","w");
	gnuplotpipe = popen("gnuplot -persistent","w");
	logFile = fopen("LogFile.txt","w");
	int i,j;
	//Event Driven Simulation Code
	int count = 3;
	World* world = newWorld();
	PhysicsObject *obj1 = newObject();
	PhysicsObject *obj2 = newObject();
	PhysicsObject *obj3 = newObject();
	PhysicsObject *obj4 = newObject();

	// obj1->currentState = newState(obj1,20,0,-2,0,0,5);
	// obj2->currentState = newState(obj2,-20,-2.5,5,0,0,2.5);
	
	obj1->currentState = newState(obj1,-1,-.5,4,1,0,0.5);
	obj2->currentState = newState(obj2,1,0,0,0,0,0.5);
	obj3->currentState = newState(obj3,-2,-1,2,5,0,0.5);
	obj4->currentState = newState(obj4,2,0,0.5,3.5,6,0.5);
	
	/*obj1->currentState = newState(obj1,2,0,-2,0,0,1);
	obj2->currentState = newState(obj2,0,2,0,-2,0,1);
	obj3->currentState = newState(obj3,-2,0,2,0,0,1);
	obj4->currentState = newState(obj4,0,-2,0,2,0,1);*/

	worldInsert(world,obj1);
	worldInsert(world,obj2);
	worldInsert(world,obj3);
	worldInsert(world,obj4);


	for (i = 0; i < world->count; ++i)
	{
		PhysicsObject *o1 = world->objects[i];
		for (j = i; j < world->count; ++j)
		{			
			PhysicsObject *o2 = world->objects[j];
			double t = getCollisionTime(o1,o2,collisions);
			//printf("Time to collide between %d and %d: %f\n", i,j,t);
		}
		computeWallCollisions(o1,collisions,mainWall);
	}
	while(collisions->heapSize>0 && curr_time<TIME_LIM)
	{
		HeapNode c = heapExtractMin(collisions);
		if(*c.isValid!=1)
			continue;
		curr_time = c.timeStamp;

		simulateCollision(c);
		PhysicsObject *o1 = c.obj1;
		for (i = 0; i < world->count; ++i)
		{			
			PhysicsObject *o2 = world->objects[i];
			double t = getCollisionTime(o1,o2,collisions);
			// printf("Time to collide between %d and %d: %f\n", i,j,t);

		}
		computeWallCollisions(c.obj1,collisions,mainWall);
		o1 = c.obj2;
		if(o1 != NULL)
		{
			for (i = 0; i < world->count; ++i)
			{
				PhysicsObject *o2 = world->objects[i];
				double t = getCollisionTime(o1,o2,collisions);
				// printf("Time to collide between %d and %d: %f\n", i,j,t);
			}
			computeWallCollisions(c.obj2,collisions,mainWall);
		}
		//printHeap(collisions);
	}
	//printf("Exiting: size: %d time: %lf\n", collisions->heapSize,curr_time);
	//heapExtractMin(collisions);
	
	char arrName[10];
	for (i = 0; i < world->count; ++i)
	{
		sprintf(arrName,"ball%d",i);
		PhysicsObject *o1 = world->objects[i];
		Path* path = o1->path;
		fprintf(matFile, "%s=[\n", arrName);

		while(path != NULL)
		{
			fprintf(temp, "%lf %lf \n",path->p.x,path->p.y);
			fprintf(matFile, "%lf %lf \n",path->p.x,path->p.y);
			// printf("Col Loc[%d]: X: %lf, Y: %lf \n",i,path->p.x,path->p.y);

			path = path->next;
		}
		fprintf(matFile, "];hold on;\n");
		fprintf(temp, "\n\n");
	}
	fprintf(temp, "7 7 \n7 -7\n-7 -7\n-7 7\n7 7\n");
	for (i = 0; i < world->count; ++i)
	{
		sprintf(arrName,"ball%d",i);
		fprintf(matFile, "plot(%s(1,:),%s(2,:));\n",arrName,arrName);
	}
	fprintf(matFile, "hold off;\n");

	for (i = 0; i < numCommand; ++i)
	{
		fprintf(gnuplotpipe, "%s \n", commandForGnuplot[i]);
	}
	//printHeap(collisions);

	fflush(gnuplotpipe);
	//printf("Count of objects in world: %d\n", world->count);
	//getCollisionTime(world->objects[0],world->objects[1]);
	fclose(temp);
	fclose(matFile);
	fclose(logFile);
	return 0;
}

Heap* newHeap(int length)
{
	Heap* h = (Heap*)malloc(sizeof(Heap));
	h->nodes = (HeapNode*)malloc(length*sizeof(HeapNode));
	h->length = length;
	h->heapSize = 0;
	return h;
}
HeapNode newHeapNode(double timeStamp, PhysicsObject* o1, PhysicsObject* o2)
{
	HeapNode a ;//= (HeapNode*)malloc(sizeof(HeapNode));
	//printf("Value to set %lf\n", timeStamp);
	a.timeStamp = timeStamp;
	a.obj1 = o1;
	a.obj2 = o2;
	a.isValid = (int*)malloc(sizeof(int));
	*a.isValid = 1;
	return a;
}
World* newWorld()
{
	World* w = (World*)malloc(sizeof(World));
	w->objects = NULL;
	w->count = 0;
	return w;
}
PhysicsObject* newObject()
{
	PhysicsObject* obj = (PhysicsObject*)malloc(sizeof(PhysicsObject));
	obj->currentState = NULL;
	obj->allEventFlags = NULL;
	obj->lastTS = 0;
	return obj;
}
PhyState* newState(PhysicsObject* o_p,double x,double y,double vx,double vy,int color,double radius)
{
	PhyState* s;
	s = (PhyState*)malloc(sizeof(PhyState));
	s->x = x;
	s->y = y;
	s->vx = vx;
	s->vy = vy;
	s->color = color;
	s->radius = radius;
	s->object = o_p;
	updatePath(o_p,x,y);
	return s;
}
void setState(PhysicsObject* obj,PhyState* s)
{
	if(s->object != obj)
	{
		return;
	}
	switchState(obj,s);
}
void updatePath(PhysicsObject* obj,double x,double y)
{
	Path* path = obj->path;
	if(path == NULL)
	{
		path = (Path*)malloc(sizeof(Path));
		path->p.x = x;
		path->p.y = y;
		path->next = NULL;
		obj->path = path;
		return;
	}
	while(path->next!=NULL)
		path = path->next;
	path->next = (Path*)malloc(sizeof(Path));
	path = path->next;
	path->p.x = x;
	path->p.y = y;
	path->next = NULL;
	//obj->path = path;
}
void plotPath(PhysicsObject* obj)
{
	Path* path = obj->path;
	while(path->next!=NULL)
	{
		printf("X: %lf, Y: %lf\n", path->p.x,path->p.y);
		path = path->next;
	}
}
void worldInsert(World* w,PhysicsObject* o)
{
	w->count++;
	if(w->count == 1)
	{
		w->objects = (PhysicsObject**)malloc(sizeof(PhysicsObject*));
		w->objects[w->count - 1] = o;
	}
	else
	{
		w->objects = (PhysicsObject**)realloc(w->objects,w->count*sizeof(PhysicsObject*));
		w->objects[w->count - 1] = o;
	}
}

void buildMinHeap(Heap* h)
{
	int i;
	for (i = ParentNode(h->length - 1); i >= 0; i--)
	{
		minHeapify(h,i);
	}
}

void minHeapify(Heap* h, int index)
{
	int l = LeftNode(index);
	int r = RightNode(index);
	int min;
	if(l<h->heapSize)
	{
		if(h->nodes[index].timeStamp > h->nodes[l].timeStamp)
			min = l;
		else
			min = index;
	}		
	else
		min = index;
	if(r<h->heapSize)
	{
		if(h->nodes[min].timeStamp > h->nodes[r].timeStamp)
			min = r;
	}		

	if(min != index)
	{
		swapNode(h,index,min);
		minHeapify(h,min);
	}
}
void swapNode(Heap* h,int this,int that)
{
	//printf("size: %d, this: %d, that: %d\n", h->heapSize,this,that);
	HeapNode a = h->nodes[this];

	h->nodes[this] = h->nodes[that];
	h->nodes[that] = a;
}
void heapSort(Heap* h)
{
	
}
HeapNode heapMin(Heap* h)
{
	return  h->nodes[0];
}
HeapNode heapExtractMin(Heap* h)
{
	HeapNode a = h->nodes[0];
	h->nodes[0] = h->nodes[h->heapSize - 1];
	h->heapSize--;
	minHeapify(h, 0);
	return a;
}
void heapDecreaseKey(Heap* h, int index, HeapNode node)
{

	if(node.timeStamp > h->nodes[index].timeStamp)
		return;
	h->nodes[index] = node;
	while(index > 0 && h->nodes[ParentNode(index)].timeStamp > h->nodes[index].timeStamp)
	{
		swapNode(h,index,ParentNode(index));
		index = ParentNode(index);
	}
}
void minHeapInsert(Heap* h,HeapNode node)
{

	//printf("**Inserting %lf\n", node.timeStamp);
	if(h->length > h->heapSize)
	{
		h->heapSize++;
	}
	else
	{
		h->length*=2;
		h->nodes = (HeapNode*)realloc(h->nodes,h->length*sizeof(HeapNode));
		h->heapSize++;
	}
	h->nodes[h->heapSize-1].timeStamp = INFINITY;
	heapDecreaseKey(h,h->heapSize - 1,node);
}
void printHeap(Heap* h)
{
	int i=0;
	printf("Size of Heap: %d\n", h->heapSize);
	printf("Length of Array: %d\n", h->length);
	while(i<h->heapSize)
	{
		printf("KEY: %lf, Flag: %d\n",h->nodes[i].timeStamp,*h->nodes[i].isValid);
		// printf("Left: %d\n ",h->nodes[LeftNode(i)].timeStamp);
		// printf("Right: %d\n\n",h->nodes[RightNode(i)].timeStamp);
		i++;

	}

}
void switchState(PhysicsObject* obj,PhyState* state)
{
	free(obj->currentState);
	obj->currentState = state;
}
void addFlag(PhysicsObject* obj,HeapNode n)
{
	FlagList* fl = obj->allEventFlags;
	if(fl == NULL)
	{
		fl = (FlagList*)malloc(sizeof(FlagList));
		fl->next = NULL;
		fl->end = fl;
		fl->flag_p = n.isValid;
		obj->allEventFlags = fl;
	}
	else
	{
		fl = fl->end;
		fl->next = (FlagList*)malloc(sizeof(FlagList));
		fl = fl->next;
		fl->end = fl;
		fl->next = NULL;
		fl->flag_p = n.isValid;
	}
}
Point getCurrentPosition(PhysicsObject* obj,double t)
{
	Point p;
	PhyState* st = obj->currentState;
	p.x = st->x + st->vx*(t - obj->lastTS);
	p.y = st->y + st->vy*(t - obj->lastTS);
	return p;
}
double getCollisionTime(PhysicsObject* obj1,PhysicsObject* obj2,Heap* collisions)
{
	if(obj1 == obj2)
		return INFINITY;
	double t = INFINITY;
	PhyState* st1 = obj1->currentState;
	PhyState* st2 = obj2->currentState;
	double xrel,yrel;
	Point p1,p2;
	p1 = getCurrentPosition(obj1,curr_time);
	p2 = getCurrentPosition(obj2,curr_time);
	xrel = p2.x - p1.x;
	yrel = p2.y - p1.y;
	double vy_rel = st2->vy - st1->vy;
	double vx_rel = st2->vx - st1->vx;
	double r1,r2;
	r1 = st1->radius;
	r2 = st2->radius;
	double time_collide = quadSolver(vx_rel*vx_rel + vy_rel*vy_rel,
		2*(vx_rel*xrel + vy_rel*yrel),xrel*xrel + yrel*yrel - (r1+r2)*(r1+r2));
	//printf("Time to collide %f\n", time_collide);
	if(time_collide != INFINITY && time_collide!=-INFINITY && time_collide > 0.000000001)
	{
		time_collide = curr_time + time_collide;		
		// printf("NHN Called from ball-ball collision.\n");
		HeapNode n = newHeapNode(time_collide,obj1,obj2);
		addFlag(obj1,n);
		addFlag(obj2,n);
		//printf("Value to set %lf\n", n.timeStamp);
		minHeapInsert(collisions,n);
	}
	return time_collide;
}
double quadSolver(double a,double b,double c)
{
	double d = (b*b - 4*a*c);
	if(d<0)
		return INFINITY;
	double s1,s2;
	s1 = (-b+sqrt(d))/(2*a);
	s2 = (-b-sqrt(d))/(2*a);
	double minT = INFINITY;
	if(s1>0)
	{
		minT = s1;
	}
	if(s2<minT && s2>0)
	{
		minT = s2;
	}

	return minT;
}
void simulateCollision(HeapNode h)
{
	fprintf(logFile, "\n\n**************************SIMULATING*A*COLLISION*************************\n" );
	fprintf(logFile,"CURRENT TIME: %lf\n", h.timeStamp);
	if(*h.isValid != 1)
	{
		return;
	}
	
	double t = h.timeStamp;
	double ux1,ux2,uy1,uy2,m;
	double vx1,vx2,vy1,vy2;
	double x1,x2,y1,y2;
	PhyState* st1 = h.obj1->currentState;

	ux1 = st1->vx;
	uy1 = st1->vy;
	Point p1;
	p1 = getCurrentPosition(h.obj1,h.timeStamp);
	x1 = p1.x;
	y1 = p1.y;
	if(h.obj2 == NULL)
	{
		t -= h.obj1->lastTS;
		fprintf(logFile,"It's an wall collision.\n");
		int WallFlag = h.WallFlag;
		fprintf(logFile, "Before Collision :\n" );
		printState(h.obj1,"Collider: \n");
		if(WallFlag == 1 || WallFlag == 3)
		{
			//TOP OR BOTTOM			
			h.obj1->currentState = newState(h.obj1,x1,y1,ux1,-uy1,st1->color,st1->radius);
			
			updatePath(h.obj1,x1,y1);
			remFutureEvents(h.obj1);
			free(st1);

		}
		else
		{
			//RIGHT OR LEFT
			h.obj1->currentState = newState(h.obj1,x1,y1,-ux1,uy1,st1->color,st1->radius);
			
			updatePath(h.obj1,x1,y1);
			remFutureEvents(h.obj1);
			free(st1);
		}
		// printState(h.obj1,"Collider 1: \n");
		h.obj1->lastTS = h.timeStamp;
		fprintf(logFile, "\nAfter Collision :\n" );
		printState(h.obj1,"Collider: \n");
		return;
	}
	
	// if(h == NULL)
	// {
	// 	printf("Null received for collision.\n");
	// }
	//printf("Collision time: %lf\n", h.timeStamp);
	fprintf(logFile, "BEFORE COLLISION:\n\n" );
	printState(h.obj1,"Collider 1: \n");
	printState(h.obj2,"Collider 2:\n");
	PhyState* st2 = h.obj2->currentState;
	double xrel,yrel;
	double t1,t2;
	t1 = t - h.obj1->lastTS;
	t2 = t - h.obj2->lastTS;
	ux2 = st2->vx;
	uy2 = st2->vy;
	Point p2;
	p2 = getCurrentPosition(h.obj2,h.timeStamp);
	x2 = p2.x;
	y2 = p2.y;
	xrel = x2 - x1;
	yrel = y2 - y1;
	
	m = yrel/xrel;

	if(m == INFINITY || m==-INFINITY)
	{
		h.obj1->currentState = newState(h.obj1,x1,y1,ux1,uy2,st1->color,st1->radius);
		h.obj2->currentState = newState(h.obj2,x2,y2,ux2,uy1,st2->color,st2->radius);
		updatePath(h.obj1,x1,y1);
		updatePath(h.obj2,x2,y2);
		remFutureEvents(h.obj1);
		remFutureEvents(h.obj2);
		free(st1);
		free(st2);
	}
	else
	{
		//printState(h.obj1,"Collider 1: \n");
		//printState(h.obj2,"Collider 2:\n");
		double uy_rel = st2->vy - st1->vy;
		double ux_rel = st2->vx - st1->vx;
		double denom = (1+m*m);
		//printf("Denom is %lf\n", denom);
		vx1 = (m*m*ux1 + m*uy_rel + ux2)/denom;
		vy1 = (m*m*uy2 + m*ux_rel + uy1)/denom;
		vx2 = (m*m*ux2 - m*uy_rel + ux1)/denom;
		vy2 = (m*m*uy1 - m*ux_rel + uy2)/denom;

		h.obj1->currentState = newState(h.obj1,x1,y1,vx1,vy1,st1->color,st1->radius);
		h.obj2->currentState = newState(h.obj2,x2,y2,vx2,vy2,st2->color,st2->radius);
		updatePath(h.obj1,x1,y1);
		updatePath(h.obj2,x2,y2);
		remFutureEvents(h.obj1);
		remFutureEvents(h.obj2);
		free(st1);
		free(st2);
	}
	
	h.obj1->lastTS = h.timeStamp;
	h.obj2->lastTS = h.timeStamp;
	fprintf(logFile, "AFTER COLLISION:\n\n" );
	printState(h.obj1,"Collider 1: \n");
	printState(h.obj2,"Collider 2:\n");
	return;
}
void remFutureEvents(PhysicsObject* obj)
{
	FlagList* fl = obj->allEventFlags;
	FlagList* temp;
	obj->allEventFlags = NULL;
	while(fl!=NULL)
	{
		*fl->flag_p = 0;
		temp = fl;
		fl=fl->next;
		free(temp);
	}
}
double computeWallCollisions(PhysicsObject* obj,Heap* collisions, Rect wall)
{
	//printf("COMPUTING WALL COLLISION\n");
	double time_collide = INFINITY;
	int wf = 0;
	Point p;
	p = getCurrentPosition(obj,curr_time);
	PhyState* st = obj->currentState;
	double r,x,y,vx,vy;
	vx = st->vx;
	vy = st->vy;
	x = p.x;
	y = p.y;
	r = st->radius;
	double tl,tt,tr,tb;
	double top,left,right,bottom;
	top = wall.top;
	right = wall.right;
	left = wall.left;
	bottom = wall.bottom;
	if(vx>0 && vy>0)
	{
		//TOP OR RIGHT
		tt = (top - y - r)/vy;
		time_collide = tt;
		wf = 1;
		tr = (right - x - r)/vx;
		if(tr<time_collide)
		{
			time_collide = tr;
			wf=2;
		}
	}
	else if(vy>0)
	{
		//TOP OR LEFT
		tt = (top - y - r)/vy;
		time_collide = tt;
		wf = 1;
		tl = (left-x+r)/vx;
		if(tl<time_collide)
		{
			time_collide = tl;
			wf=4;
		}
	}
	else if(vx>0)
	{
		//BOTTOM OR RIGHT
		tb = (bottom-y+r)/vy;
		time_collide = tb;
		wf = 3;

		tr = (right - x -r)/vx;
		if(tr<time_collide)
		{
			time_collide = tr;
			wf = 2;
		}
	}
	else
	{
		//BOTTOM OR LEFT
		tb = (bottom-y+r)/vy;
		time_collide = tb;
		wf = 3;
		tl = (left-x+r)/vx;
		if(tl<time_collide)
		{
			time_collide = tl;
			wf = 4;
		}
			
	}
	time_collide += curr_time;
	// printf("WALL COLLISION DELAY: %lf and WF: %d\n", time_collide, wf);
	if(wf!=0 && curr_time<time_collide)
	{
		// printf("Value of time_collide %lf\n", time_collide);
		// printf("NHN called from wall.\n");
		HeapNode n = newHeapNode(time_collide,obj,NULL);
		addFlag(obj,n);
		n.WallFlag = wf;
		//printf("Value to set %lf\n", n.timeStamp);
		minHeapInsert(collisions,n);
	}
	return time_collide;
}
void printState(PhysicsObject* obj,char* message)
{
	if(obj == NULL)
		return;
	PhyState* st = obj->currentState;
	fprintf(logFile,"%s\n", message);
	fprintf(logFile,"State: x: %lf, y: %lf, vx: %lf, vy: %lf\n", 
		st->x,st->y,st->vx,st->vy);
	fprintf(logFile,"Ts: %lf\n", obj->lastTS);
}
