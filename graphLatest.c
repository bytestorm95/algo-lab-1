#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#define WHITE 100
#define GRAY 101
#define BLACK 102


typedef struct _node{
	int key;
	//REMOVE FROM GRAPH IF SET TO 0 TO MAKE A EULER FOREST
	//DEFAULT EULERFLAG = 1;
	//CALL MARKEDGE TO PREVENT IT FROM REMOVAL
	int EulerFlag; 
	struct _node* next;
} Node;



typedef struct _intNode{
	int key;
	struct _intNode* parent;
} Integer;
typedef struct _intStack{
	Integer* top;
} Stack;



typedef struct _dfs{
	int* color; //DEFAULT WHITE
	int* d; //DEFAULT INT_MAX
	int* pi; //DEFAULT -1
	int* f; //DEFAULT INT_MAX
} DFS;

typedef struct _graph{
	Node** AdjList;
	Node** lastNode;
	Stack* TopologicalSort;
	Stack* Circuit;
	DFS* dfs;
	int* inDegree;
	int* outDegree;
	int* fixEdgeArr;
	int cycleFlag;//DEFAULT FALSE -- ACYCLIC GRAPH
	int numVertex;
} Graph;

typedef struct _gnode{
	Graph* graph;
	struct _gnode* next;
} GraphNode;

typedef GraphNode* GraphList;

Graph* newGraph(int numVertex);
Graph* createRandomGraph(int numVertex, double prob);
Node* newNode(int key);
int joinEdge(Graph* graph,int u,int v);
void printAdjList(Graph* graph,int numVertex);
void initRandom();
void doDFS(Graph* graph,int numVertex);
void DFS_Visit(Graph* graph,int u);
DFS* DFSInit(int numVertex);
Stack* newStack();
void Push(Stack* s,int key);
Integer* Pop(Stack* s);
void printStack(Stack* s);
void VizTopologicalSort(Graph* graph);
void VizEulerianCycle(Graph* graph,int numVertex);
void FindEuler(Graph* graph,Stack* stack,int currentV,Graph* rescue);
void removeFirstEdge(Graph* graph,int currentV);
Graph* GTranspose(Graph* g);
void SCC(Graph* graph);
void SCC_DFS_Visit(Graph* gt, int u, Graph* graph, int* withinCycle);
GraphNode* newGraphNode();
void markEdge(Graph* graph,int u,int v);
void removeEdge(Graph* graph,int u,int v);
void FixGraph(Graph* graph);
int DFS_Remover(Graph* graph,int u,int source);

int TIME;

int main(int argc,char* argv[])
{
	printf("ENTER '-M' FLAG FOR MANUAL ENTRY\n\n");
	int numVertex;
	double prob;

	printf("Enter Number of vertices: \n");
	scanf("%d",&numVertex);

	int i,u,v,numEdges;
	Graph* graph;
	Graph* gt;
	if (argc > 1)
	{
		printf("How many edges are there?\n");
		scanf("%d",&numEdges);
		printf("Enter %d edges(one edge per line):-\n", numEdges);
		graph = newGraph(numVertex);
		for (i = 0; i < numEdges; ++i)
		{
			scanf("%d %d",&u,&v);
			joinEdge(graph,u-1,v-1);
		}
		printAdjList(graph,numVertex);

		graph->dfs = DFSInit(numVertex);
		doDFS(graph,numVertex);
		gt = GTranspose(graph);
		// FixGraph(graph);
		if(graph->cycleFlag == 0)
		{
			//ACYCLIC GRAPH
			//PRINT THE TOPOLOGICALLY SORTED STACK
			VizTopologicalSort(graph);
		}
		else
		{
			//LOOK FOR AN EULER TOUR IF EXISTS
			printf("Cycle Detected!!!\n");
			VizEulerianCycle(graph,numVertex);
		}
	}
	else
	{
		initRandom();
		printf("Enter Probability: \n");
		scanf("%lf",&prob);
		graph = createRandomGraph(numVertex,prob);
		printAdjList(graph,numVertex);
		graph->dfs = DFSInit(numVertex);
		doDFS(graph,numVertex);
		gt = GTranspose(graph);
		if(graph->cycleFlag == 0)
		{
			//ACYCLIC GRAPH
			//PRINT THE TOPOLOGICALLY SORTED STACK
			VizTopologicalSort(graph);
		}
		else
		{
			//LOOK FOR AN EULER TOUR IF EXISTS
			printf("Cycle Detected!!!\n");
			VizEulerianCycle(graph,numVertex);
		}
	}
	
	return 0;
}

Node* newNode(int key)
{
	Node* node = (Node*)malloc(sizeof(Node));
	node->key = key;
	node->next = NULL;
	node->EulerFlag = 0;
	return node;
}

Graph* newGraph(int numVertex)
{
	//ALLOCATE MEMORY FOR GRAPH
	Graph* graph;
	graph = (Graph*)malloc(sizeof(Graph));
	int i,j;
	graph->AdjList = (Node**)malloc(numVertex*sizeof(Node*));
	graph->lastNode = (Node**)malloc(numVertex*sizeof(Node*));
	graph->inDegree = (int*)malloc(numVertex*sizeof(int));
	graph->outDegree = (int*)malloc(numVertex*sizeof(int));
	graph->fixEdgeArr = (int*)malloc(numVertex*sizeof(int));
	for (i = 0; i < numVertex; ++i)
	{
		graph->AdjList[i] = NULL;
		graph->lastNode[i] = NULL;
		graph->inDegree[i] = 0;
		graph->outDegree[i] = 0;
		graph->fixEdgeArr[i] = 0;
	}
	graph->TopologicalSort = newStack();
	graph->Circuit = newStack();
	graph->dfs = NULL;
	graph->cycleFlag = 0;//FALSE
	graph->numVertex = numVertex;
	return graph;
}
GraphNode* newGraphNode()
{
	GraphNode* g = (GraphList)malloc(sizeof(GraphNode));
	g->next = NULL;
	g->graph = NULL;
}
Graph* createRandomGraph(int numVertex, double prob)
{
	//ALLOCATE MEMORY FOR GRAPH
	Graph* graph = newGraph(numVertex);
	int i,j;
	//RANDOMLY JOIN SOME EDGES BETWEEN THE VERTICES IN THE GRAPH 

	for (i = 0; i < numVertex; ++i)
	{
		for (j = 0; j < numVertex; ++j)
		{
			if(i==j)
				continue;
			//JOIN DIRECTED EDGE (i,j) WITH A PROBABILITY prob
			if(drand48() < prob)
				joinEdge(graph,i,j);
		}
	}
	return graph;

}
int joinEdge(Graph* graph,int u,int v)
{
	//RETURNS 1 IF EDGE IS JOINED
	//CHECK IF THE DIRECTED EDGE (V,U) EXISTS
	Node* search = graph->AdjList[v];
	while(search != NULL)
	{
		if(search->key == u)
		{
			return 0;
		}
		search = search->next;
	}

	//CREATE THE NEW EDGE (U,V)
	if(graph->lastNode[u] == NULL)
	{
		graph->AdjList[u] = newNode(v);
		graph->lastNode[u] = graph->AdjList[u];
	}
	else
	{
		graph->lastNode[u]->next = newNode(v);
		graph->lastNode[u] = graph->lastNode[u]->next;
	}
	graph->inDegree[v] = graph->inDegree[v] + 1;
	graph->outDegree[u] = graph->outDegree[u] + 1;
	return 1;
}
DFS* DFSInit(int numVertex)
{
	DFS* dfs = (DFS*)malloc(sizeof(DFS));
	dfs->color = (int*)malloc(numVertex*sizeof(int));
	dfs->pi = (int*)malloc(numVertex*sizeof(int));
	dfs->f = (int*)malloc(numVertex*sizeof(int));
	dfs->d = (int*)malloc(numVertex*sizeof(int));
	int i;
	for (i = 0; i < numVertex; ++i)
	{
		dfs->color[i] = WHITE;
		dfs->d[i] = INT_MAX;
		dfs->f[i] = INT_MAX;
		dfs->pi[i] = -1;
	}
	return dfs;

}
void doDFS(Graph* graph,int numVertex)
{
	int i;
	DFS* dfsResult = graph->dfs;
	graph->TopologicalSort = newStack();
	TIME = 0; //GLOBAL TIMER
	for (i = 0; i < numVertex; ++i)
	{
		if(dfsResult->color[i] == WHITE)
		{
			DFS_Visit(graph,i);
		}		
	}

}
void DFS_Visit(Graph* graph,int u)
{
	TIME = TIME+1;
	DFS* dfsResult = graph->dfs;
	dfsResult->d[u] = TIME;
	dfsResult->color[u] = GRAY;

	Node* header = graph->AdjList[u];
	while(header != NULL)
	{
		int v = header->key;
		if(dfsResult->color[v] == WHITE)
		{
			// printf("Recursive @ %d\n",u+1);
			dfsResult->pi[v] = u;
			DFS_Visit(graph,v);
		}
		else if(dfsResult->color[v] == GRAY)
		{
			//CYCLE DETECTED
			graph->cycleFlag = 1;
		}

		header = header->next;
	}
	dfsResult->color[u] = BLACK;
	TIME = TIME+1;
	dfsResult->f[u] = TIME;
	Push(graph->TopologicalSort,u);
	// printf("%d (%d,%d)\n", u+1, dfsResult->d[u], dfsResult->f[u]);
}
void printAdjList(Graph* graph,int numVertex)
{
	int i;
	for (i = 0; i < numVertex; ++i)
	{
		Node* header = graph->AdjList[i];
		printf("Vertex: %d (", i+1);
		while(header != NULL)
		{
			printf(" %d-> ", header->key+1);
			header = header->next;
		}
		printf(" NULL )\n");
	}
}
void initRandom()
{
	srand48(time(NULL));
}

//FUNCTIONS FOR STACK DATA STRUCTURE
Stack* newStack()
{
	Stack* s = (Stack*)malloc(sizeof(Stack));
	s->top = NULL;
	return s;
}
void Push(Stack* s,int key)
{
	if(s->top != NULL)
	{
		Integer* a = (Integer*)malloc(sizeof(Integer));
		a->key = key;
		a->parent = s->top;
		s->top = a;
	}
	else
	{
		Integer* a = (Integer*)malloc(sizeof(Integer));
		a->key = key;
		a->parent = NULL;
		s->top = a;
	}
}
Integer* Pop(Stack* s)
{
	if(s->top != NULL)
	{
		Integer* a = s->top;
		s->top = s->top->parent;
		return a;
	}
	return NULL;	
}
void printStack(Stack* s)
{
	Integer* u = NULL;
	while((u = Pop(s)) != NULL)
	{
		printf("%d\t", u->key + 1);
		free(u);
	}
	printf("\n");
}
void VizTopologicalSort(Graph* graph)
{
	printf("Topological Sort: \n");
	DFS* dfs = graph->dfs;
	Stack* s = graph->TopologicalSort;
	Integer* u = NULL;
	while((u = Pop(s)) != NULL)
	{
		printf("%d\t", u->key + 1);
		free(u);
	}
	printf("\n");
}
void VizEulerianCycle(Graph* graph,int numVertex)
{
	int i;
	int EPathFlag1 = 0;
	int EPathFlag2 = 0;
	int start = 0;
	printAdjList(graph,graph->numVertex);

	//EPathFlag1 = 1 FOR OUT>IN HAS BEEN FOUND
	//EPathFlag2 = 1 FOR OUT<IN HAS BEEN FOUND
	for (i = 0; i < numVertex; ++i)
	{
		if(graph->inDegree[i] != graph->outDegree[i])
		{
			// if(EPathFlag1 > 0 || EPathFlag2 > 0)
			// {
			// 	printf("EULERIAN TOUR ISN'T POSSIBLE\n");
			// 	return;
			// }

			if(graph->inDegree[i] > graph->outDegree[i] )
			{
				EPathFlag2++;
			}
			else if(graph->inDegree[i] < graph->outDegree[i])
			{
				EPathFlag1++;
				start = i;
			}
			
		}
	}
	if(EPathFlag1 == EPathFlag2 && EPathFlag2 == 0)
	{
		printf("EULERIAN TOUR IS POSSIBLE\n");
		Stack* s = newStack();
		Graph* rescue = newGraph(numVertex);
		for (i = 0; i < numVertex; ++i)
		{
			if(graph->AdjList[i] != NULL)
			{
				FindEuler(graph,s,i,rescue);
				printStack(graph->Circuit);
				graph->Circuit = newStack();
				s = newStack();
				i = 0;
			}
		}
		printStack(graph->Circuit);
		
		// printAdjList(graph,graph->numVertex);
	}
	else
	{
		printf("EULERIAN TOUR ISN'T POSSIBLE\n");
		SCC(graph);
		FixGraph(graph);
		scanf("%d",&i);
		VizEulerianCycle(graph,numVertex);
	}
}
void FindEuler(Graph* graph,Stack* stack,int currentV,Graph* rescue)
{
	//Graph rescue holds the removed edges
	//STACK IS THE AUXILLIARY STACK TO HOLD THE VERTICES
	
	// If current vertex has no out-going edges (i.e. neighbors)
	// - add it to circuit, remove the last vertex from 
	// the stack and set it as the current one. 

	// Otherwise (in case it has out-going edges, i.e. neighbors) - 
	// add the vertex to the stack, take any of its neighbors, 
	// remove the edge between that vertex and selected neighbor, 
	// and set that neighbor as the current vertex.

	if(graph->AdjList[currentV] == NULL)
	{
		Push(graph->Circuit,currentV);
		Integer* a = Pop(stack);
		if(a != NULL)
		{
			FindEuler(graph,stack,a->key,rescue);
		}
	}
	else
	{
		Push(stack,currentV);
		Node* neighbour = graph->AdjList[currentV];
		//REMOVE THE CORRESPODING EDGE
		removeFirstEdge(graph,currentV);
		// printf("Join Stat: %d, %d (%d)\n", currentV,neighbour->key, joinEdge(rescue,currentV,neighbour->key));
		FindEuler(graph,stack,neighbour->key,rescue);
		free(neighbour);
	}
	
}
void FixGraph(Graph* graph)
{
	int i;
	graph->dfs = DFSInit(graph->numVertex);
	DFS* dfsResult = graph->dfs;
	graph->TopologicalSort = newStack();
	TIME = 0; //GLOBAL TIMER
	for (i = 0; i < graph->numVertex; ++i)
	{
		if(dfsResult->color[i] == WHITE)
		{
			DFS_Remover(graph,i,i);
		}		
	}
}
int DFS_Remover(Graph* graph,int u,int source)
{
	TIME = TIME+1;
	DFS* dfsResult = graph->dfs;
	dfsResult->d[u] = TIME;
	dfsResult->color[u] = GRAY;
	int ret = 0;

	Node* header = graph->AdjList[u];
	while(header != NULL)
	{
		int v = header->key;
		if(dfsResult->color[v] == WHITE)
		{
			// printf("Recursive @ %d\n",u+1);
			if(ret == 0)
			{
				dfsResult->pi[v] = u;
				ret = DFS_Remover(graph,v,source);
				if(ret == 0)
				{
					removeEdge(graph,u,v);
				}
			}
			else if(ret == 1)
			{
				removeEdge(graph,u,v);
			}
				
		}
		else if(dfsResult->color[v] == GRAY || dfsResult->color[v] == BLACK)
		{
			//FAILED CYCLE
			if(v != source)
			{
				removeEdge(graph,u,v);
			}
			else if(v == source)
			{
				ret = 1;
			}
			graph->cycleFlag = 1;
		}

		header = header->next;
	}
	dfsResult->color[u] = BLACK;
	TIME = TIME+1;
	dfsResult->f[u] = TIME;
	Push(graph->TopologicalSort,u);
	return ret;
}
Graph* GTranspose(Graph* g)
{
	//MAKE SURE U HAVE RUN DFS BEFORE CALLING THIS FUNCTION
	//NEED TO USE TOP-SORT HERE
	Graph* gt = newGraph(g->numVertex);
	Node* current;
	int i;
	for (i = 0; i < g->numVertex; ++i)
	{
		current=g->AdjList[i];
		while(current != NULL)
		{
			joinEdge(gt,current->key,i);
			current = current->next;
		}
	}
	//printAdjList(gt,gt->numVertex);
	return gt;
}
void SCC(Graph* graph)
{
	Graph* gt = GTranspose(graph);

	// GraphList glist = newGraphNode();
	// GraphList currentNode = glist;
	// currentNode->graph = newGraph(graph->numVertex);

	//DFS OF GTRANSPOSE STARTS
	int i;
	gt->dfs = DFSInit(gt->numVertex);
	DFS* dfsResult = gt->dfs;

	TIME = 0; //GLOBAL TIMER
	Integer* u;
	int* withinCycle = (int*)malloc(graph->numVertex*sizeof(int));
	for (i = 0; i < graph->numVertex; ++i)
	{
		withinCycle[i] = 0;
	}
	for (i = 0; i < gt->numVertex; ++i)
	{
		u = Pop(graph->TopologicalSort);
		if(u == NULL)
			break;
		while(withinCycle[u->key] == 1)
		{
			u = Pop(graph->TopologicalSort);
			if(u==NULL)
				break;
		}
		if(u!= NULL && dfsResult->color[u->key] == WHITE)
		{
			// printf("Clalling SCC DFS ofr %d\n", u->key+1);
			SCC_DFS_Visit(gt,u->key,graph,withinCycle);
			withinCycle[u->key] = 1;

		}

		// currentNode->next = newGraphNode();
		// currentNode = currentNode->next;
		// currentNode->graph = newGraph(graph->numVertex);
	}

	printf("PRINTING SCCs : \n\n");

	for (i = 0; i < graph->numVertex; ++i)
	{
		Node* header = graph->AdjList[i];
		// printf("Vertex: %d (", i+1);
		while(header != NULL)
		{
			if(header->EulerFlag==0)
			{
				// printf("(Remove)");
				removeEdge(graph,i,header->key);
			}
			// printf(" %d-> ", header->key+1);
			
			header = header->next;
		}
		// printf(" NULL )\n");
	}
	printf("Buggy\n");
	//BUILD TOPOLOGICAL SORT STACK AGAIN
	doDFS(graph,graph->numVertex);
	// currentNode = glist;
	// while(currentNode != NULL)
	// {
	// 	VizEulerianCycle(currentNode->graph,graph->numVertex);
	// 	currentNode = currentNode->next;
	// }
}
void SCC_DFS_Visit(Graph* gt, int u, Graph* graph, int* withinCycle)
{
	// TIME = TIME+1;
	DFS* dfsResult = gt->dfs;
	// dfsResult->d[u] = TIME;
	dfsResult->color[u] = GRAY;

	Node* header = gt->AdjList[u];
	while(header != NULL)
	{
		int v = header->key;
		// printf("called by %d ,Recursive @ %d\n",u+1,v+1);
		// printf("Color is %d \n", dfsResult->color[v]);
		withinCycle[v] = 1;
		if(dfsResult->color[v] == WHITE)
		{
			dfsResult->pi[v] = u;
			// header->EulerFlag = 1;
			markEdge(graph,v,u);
			SCC_DFS_Visit(gt,v,graph,withinCycle);
		}
		else if(dfsResult->color[v] == GRAY)
		{
			//CYCLE DETECTED

			markEdge(graph,v,u);
			gt->cycleFlag = 1;
		}
		header = header->next;
	}
	dfsResult->color[u] = BLACK;
	// TIME = TIME+1;
	// dfsResult->f[u] = TIME;
	// Push(gt->TopologicalSort,u);
	// printf("%d (%d,%d)\n", u+1, dfsResult->d[u], dfsResult->f[u]);
}
void removeFirstEdge(Graph* graph,int currentV)
{
	graph->AdjList[currentV] = graph->AdjList[currentV]->next;
}
void markEdge(Graph* graph,int u,int v)
{
	Node* curr = graph->AdjList[u];
	while(curr->key != v)
	{
		curr=curr->next;
	}
	// printf("Marking Edge %d->%d\n", u+1,v+1);
	curr->EulerFlag = 1;
}
void removeEdge(Graph* graph,int u,int v)
{
	Node* curr = graph->AdjList[u];
	Node* last = NULL;
	while(curr->key != v)
	{
		last = curr;
		curr=curr->next;
	}
	if(last != NULL)
	{
		last->next = curr->next;
	}
	else
	{

		graph->AdjList[u] = curr->next;
	}
	graph->inDegree[v]--;
	graph->outDegree[u]--;
	// printf("Marking Edge %d->%d\n", u+1,v+1);
	// curr->EulerFlag = 1;
	free(curr);
}