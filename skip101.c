#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <limits.h>
#define MAXL 16
typedef int Key;
typedef char* string;
/*
.	->	->	->	.
.	->	.	->	.
.	->	.	->	.
.	->	.	->	.

*/

// typedef struct _list{
// 	struct _node** forward;
// 	int level;
// } List;


typedef struct _node{
	Key key;
	struct _node** forward;
	string data;
	int level;
} Node;
typedef Node List;
typedef List SkipList;
void initRandom();
SkipList* newSkipList();
Node* newNode(Key key,string data);
Node* copyNode(Node* src);
void insert(SkipList* slist,Key key,string data);
Node* search(SkipList* slist,Key key);
void delete(SkipList* slist,Key key);
void printNode(Node* n);

//Merges two skiplists and return the merged list
SkipList* merge(SkipList* slist1,SkipList* slist2);

void ioTraverse(SkipList* slist);
int randomLevel(int MaxLevel, double p);


int main(int argc,char* argv[])
{
	initRandom();
	if(argc == 1)
	{
		printf("Run with one of the following options: \n");
		printf("\tI slIDx ky 'Data'\n");
		printf("\tS slIDx ky\n");
		printf("\tD slIDx ky\n");
		printf("\tM slIDx1 slIDx2\n");
		printf("\tT slIDx\n");
		return;
	}
	int i;
	Node* result = NULL;

	SkipList* slist[10];
	for (i = 0; i < 10; ++i)
	{
		slist[i] = newSkipList();
	}	
	int slIDx,key;
	int slIDx2,slIDx1;
	string data;
	printf("\n");
	printf("---------SKIPLIST PROGRAM----------\n");
	for (i = 1; i < argc;)
	{
		switch(**(argv+i))
		{
			case 'I':
				i++;
				slIDx = atoi(*(argv+i));
				i++;
				key = atoi(*(argv+i));
				i++;
				data = *(argv+i);
				i++;
				printf("\nInserting %d to %d\n", key, slIDx);
				insert(slist[slIDx],key, data);
			break;
			case 'S':
				i++;
				slIDx = atoi(*(argv+i));
				i++;
				key = atoi(*(argv+i));
				i++;
				printf("\nSEARCHING FOR %d in %d:\n", key, slIDx);
				result = search(slist[slIDx],key);
				if(result != NULL)
				{
					printNode(result);	
				}
				else
				{
					printf("No results found.\n");
				}
			break;
			case 'D':
				i++;
				slIDx = atoi(*(argv+i));
				i++;
				key = atoi(*(argv+i));
				i++;
				printf("\nDELETE %d from %d\n", key, slIDx);
				delete(slist[slIDx],key);

			break;
			case 'T':
				i++;
				slIDx = atoi(*(argv+i));
				printf("\nPRINTING ALL DATA [ID = %d]\n",slIDx);
				printf("-------------------------------\n\n");
				ioTraverse(slist[slIDx]);
				i++;
			break;
			case 'M':
				i++;
				slIDx1 = atoi(*(argv+i));
				i++;
				slIDx2 = atoi(*(argv+i));
				slist[9] = merge(slist[slIDx1],slist[slIDx2]);
				printf("\nPRINTING THE MERGED LIST [ID = 9]\n");
				printf("-------------------------------\n\n");
				ioTraverse(slist[9]);
				i++;
			break;
		}
	}	
	return 0;
}
void initRandom()
{
	srand48(time(NULL));
}
int randomLevel(int MaxLevel, double p)
{
	int level = 1;
	while(drand48() < p && level<MaxLevel)
		level+=1;
	return level;
}
SkipList* newSkipList()
{
	SkipList* slist = (SkipList*)malloc(sizeof(SkipList));
	slist->forward = (Node**)malloc(MAXL*sizeof(Node*));
	slist->level = 1;
	int i;
	Node* n = newNode(INT_MAX,"");
	n->level = MAXL - 1;
	for (i = 0; i < MAXL; ++i)
	{
		slist->forward[i] = n;
	}
	return slist;
}
Node* newNode(Key key,string data)
{
	int level = randomLevel(MAXL,0.5);
	Node *n = (Node*)malloc(sizeof(Node));
	n->forward = (Node**)malloc(level*sizeof(Node*));
	n->key = key;
	n->level = level;
	n->data = (string)malloc((1+strlen(data))*sizeof(char));
	strcpy(n->data,data);
	return n;
}
Node* copyNode(Node* src)
{
	//CREATE A CLONE OF A NODE
	Key key;
	string data;
	int level;
	key = src->key;
	data = src->data;
	level = src->level;
	//ALLOCATE MEMORY
	Node *n = (Node*)malloc(sizeof(Node));
	n->forward = (Node**)malloc(level*sizeof(Node*));
	n->key = key;
	n->level = level;
	n->data = (string)malloc((1+strlen(data))*sizeof(char));
	strcpy(n->data,data);
	//PROPERLY REPLICATE THE LEVEL FORWARDS
	int i;
	for (i = 0; i < src->level; ++i)
	{
		n->forward[i] = src->forward[i];
	}

	return n;
}
void printNode(Node* n)
{
	printf("%d\t\t%s\t\t\t%d\t\n", n->key, n->data, n->level);
}
void insert(SkipList* slist,Key key,string data)
{
	Node* update[MAXL];
	

	int level = slist->level;
	int i;

	Node* x = slist->forward[level-1];
	for (i = level - 1; i >= 0; --i)
	{
		//GET THE LOWEST KEY VALUE AT LEVEL i
		x = slist->forward[i]; 
		if(x->key > key)
		{
			update[i] = NULL;
			// printf("Continuing @ %d\n",i);
			continue;
		}
		// printf("That: %d This: %d @ lvl = %d\n", x->key, key, i );
		if(x->key == key)
		{
			// printf("Match: %d & %d \n", x->key,key);
			// printf("Data: %s\n", data);
			// printf("xData: %s\n", x->forward[0]->data);
			strcat(x->data,data);
			//KEY ALREADY EXISTS
			//CONCATENATE THE DATA
			return;
		}
		while(x->forward[i]->key < key)
		{
			x = x->forward[i];
		}
		// printf("That: %d This: %d @ lvl = %d\n", x->key, key, i );
		
		update[i] = x;
	}
	level = slist->level;
	if(update[0] != NULL)
	{
		if(x->forward[0]->key == key)
		{
			//KEY ALREADY EXISTS
			//CONCATENATE THE DATA


			// printf("Match: %d & %d \n", x->forward[0]->key,key);
			// printf("%s\n", x->forward[0]->data);
			strcat(x->forward[0]->data,data);
			// printf("%s\n", x->forward[0]->data);
			
			return;
		}
	}
	//INSERT THE NEW NODE 
	Node* n = newNode(key,data);
	// printf("Inserted: \n");
	// printNode(n);
	if(n->level > level)
	{
		for (i = level; i < n->level; ++i)
		{
			update[i] = NULL;
		}
		slist->level = n->level;
	}
	for(i=0;i<n->level;i++)
	{
		if(update[i] != NULL)
		{
			n->forward[i] = update[i]->forward[i];
			update[i]->forward[i] = n;
		}
		else
		{
			n->forward[i] = slist->forward[i];
			slist->forward[i] = n;
		}
	}
}
void delete(SkipList* slist,Key key)
{
	Node* update[MAXL];
	

	int level = slist->level;
	int i;

	Node* x = slist->forward[level-1];
	for (i = level - 1; i >= 0; --i)
	{
		//GET THE LOWEST KEY VALUE AT LEVEL i
		x = slist->forward[i]; 
		if(x->key >= key)
		{
			update[i] = NULL;
			continue;
		}
		while(x->forward[i]->key < key)
		{
			x = x->forward[i];
		}
		
		update[i] = x;
	}
	Node* n = update[0]==NULL?slist->forward[0]:update[0]->forward[0];
	level = slist->level;
	if(n->key == key)
	{
		for (i = 0; i < level; ++i)
		{
			if(update[i] != NULL)
			{
				if(update[i]->forward[i] != n)
					break;
				update[i]->forward[i] = n->forward[i];
			}
			else
			{
				if(slist->forward[i] != n)
					break;
				slist->forward[i] = n->forward[i];
			}
		}
		free(n);

		while(slist->level>1 && slist->forward[slist->level-1]->key == INT_MAX)
			slist->level = slist->level-1;

	}
	else
	{
		printf("Key Not Found!!\n");
	}

}
Node* search(SkipList* slist,Key key)
{
	int level = slist->level;
	int i;
	Node* x = slist;
	for (i = level - 1; i >= 0; --i)
	{
		while(x->forward[i]->key < key)
			x = x->forward[i];
		if(x->forward[i]->key == key)
			return x->forward[i];			
	}
	return NULL;
}
void ioTraverse(SkipList* slist)
{
	int i;
	Node* x;
	printf("LEVELS OF SKIP LIST\n");
	for(i=slist->level-1;i>=0;i--)
	{
		x=slist->forward[i];
		printf("Lvl-%d-:", i);
		while(x->key != INT_MAX)
		{
			printf("\t%d", x->key);
			x=x->forward[i];
		}
		printf("\n");
	}
	x = slist->forward[0];
	printf("Level of list: %d\n", slist->level);
	printf("..................................................\n");
	printf("KEY\t\tDATA\t\t\tLEVEL\t\n");
	printf("..................................................\n");
	
	while(x!=NULL)
	{
		if(x->key == INT_MAX)
			return;
		printNode(x);
		x=x->forward[0];
	}
	printf("..................................................\n");
}
SkipList* merge(SkipList* slist1,SkipList* slist2)
{
	SkipList* merged = newSkipList();
	Node* end = merged->forward[0];
	Node* x;
	int i,currLvl;

	Node* history1[MAXL];
	Node* history2[MAXL];
	Node* update[MAXL];

	for (i = 0; i < MAXL; ++i)
	{
		history1[i] = slist1->forward[i];
		history2[i] = slist2->forward[i];
		update[i] = merged;
	}
	while(history1[0]->key != INT_MAX || history2[0]->key != INT_MAX)
	{
		//BOTH THE LISTS ARE NON EMPTY
		if(history1[0]->key == history2[0]->key)
		{
			//SAME KEY EXISTS IN BOTH THE LIST
			currLvl = 0;
			x=copyNode(history1[0]);

			x->data = (string)realloc(x->data,(strlen(x->data)+strlen(history2[0]->data) + 2)*sizeof(char));
			strcat(x->data,history2[0]->data);
			int c = x->level;
			for(i=0;i<c;i++)
			{
				history1[i]=x->forward[i];

				//SET THE FORWARD LEVELS OF THE NEW NODE IN THE MERGED LIST
				x->forward[i] = end;
				update[i]->forward[i] = x;
				update[i] = x;
			}
			c = history2[0]->level;
			for(i=0;i<c;i++)
			{
				history2[i]=history2[i]->forward[i];
			}
		}
		//DIFFERENT KEYS 
		else if(history1[0]->key < history2[0]->key)
		{
			currLvl = 0;
			x=copyNode(history1[0]);			

			for(i=0;i<x->level;i++)
			{
				//STORE THE FIRST ELEMENT OF LEVEL i IN HISTORY
				history1[i]=x->forward[i];
				//UNLINK THE HEADER FROM X
				//slist1->forward[i] = x->forward[i];
				//APPEND X TO THE MERGED ARRAY
				x->forward[i] = end;
				//UPDATE THE LEVELS OF THE LAST ADDED NODES
				update[i]->forward[i] = x;
				//STORE THE POIINTER TO THE LAST ADDED NODE IN THE UPDATE LIST
				update[i] = x;
			}
		}
		else
		{
			currLvl = 0;
			x=copyNode(history2[0]);
			
			for(i=0;i<x->level;i++)
			{
				history2[i]=x->forward[i];
				//slist2->forward[i] = x->forward[i];
				x->forward[i] = end;
				update[i]->forward[i] = x;
				update[i] = x;
			}
		}
	}
	// if(slist1->forward[0]->key != INT_MAX)
	// {
	// 	//SLIST2 IS EMPTY NOW
	// 	for (i = 0; i < MAXL; ++i)
	// 	{
	// 		update[i]->forward[i] = history1[i];
	// 	}
	// }
	// else if(slist2->forward[0]->key != INT_MAX)
	// {
	// 	//SLIST1 IS EMPTY NOW
	// 	for (i = 0; i < MAXL; ++i)
	// 	{
	// 		update[i]->forward[i] = history2[i];
	// 	}
	// }
	i=0;
	while(i<MAXL && merged->forward[i]->key != INT_MAX)
	{
		i++;
	}
	merged->level = i;
	return merged;

}
