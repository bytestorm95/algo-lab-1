#include <stdio.h>
#include <stdlib.h>

typedef struct _node{
	struct _node* left;
	struct _node* right;
	struct _node* p;
	char* data;
	int height;
	int key;
} Node;
typedef struct _listItem
{
	Node* this;
	struct _listItem* next;
} ListItem;
typedef struct _list
{
	ListItem* start;
	ListItem* end;
} List;

Node* newNode(int k,char *data);
ListItem* newListItem(Node* a);
List* newList(ListItem* start,ListItem* end);
void listInsert(List* l,ListItem* li);
void listPrint(List* l);
int fixHeight(Node* node);
Node* insert(Node* _tree, Node* _n); //No I/O operations
void search(Node* _tree,int k,List* result); //No I/O operations
Node* delete(Node* _tree,int k); //No I/O operations
Node* fixImbalance(Node* _tree,Node* _n);
void ioTraverse(Node* _tree);
int getHImbalance(Node* _n);
Node* leftRotate(Node* n);
Node* rightRotate(Node* n);
void printTree(Node* _tree);

int main(int argc,char *argv[])
{
	int i;
	if(argc == 1)
	{
		printf("Run with one of the following options: \n");
		printf("I ky 'TAG DATA'\n");
		printf("S ky\n");
		printf("D ky\n");
		printf("T\n");
	}
	Node* tree = NULL;
	int key;
	printf("\n");
	printf("----------------AVL TREE PROGRAM------------------\n");
	for (i = 1; i < argc;)
	{
		switch(**(argv+i))
		{
			case 'I':
				i++;
				key = atoi(*(argv+i));
				i++;
				char* data = *(argv+i);
				i++;
				//printf("Inserting %s at %d\n", data, key);
				tree = insert(tree,newNode(key,data));
			break;
			case 'S':
				i++;
				key = atoi(*(argv+i));
				printf("SEARCHING FOR %d :\n", key);
				List* result = newList(NULL,NULL);
				search(tree,key,result);
				listPrint(result);
				i++;
			break;
			case 'D':
				i++;
				key = atoi(*(argv+i));
				printf("DELETE %d :\n", key);
				tree = delete(tree,key);
				i++;
			break;
			case 'T':
				printf("--------------------------------------------------\n");
				printf("-----------------PRINTING ALL DATA----------------\n");
				printf("--------------------------------------------------\n");
				printf("ROLL\t\tDATA\t\tHEIGHT IN AVL TREE\n");
				printf("--------------------------------------------------\n");
				ioTraverse(tree);
				printf("--------------------------------------------------\n");
				i++;
			break;
			case 'P':
				printf("--------------------------------------------------\n");
				printTree(tree);
				i++;
			break;
		}		
	}
	return 0;
}

Node* newNode(int k,char* data)
{
	Node *a = (Node*)malloc(sizeof(Node));
	a->left = a->right = a->p = NULL;
	a->key = k;
	a->height = 0;
	a->data = data;
	return a;
}
ListItem* newListItem(Node* a)
{
	ListItem* li= (ListItem*)malloc(sizeof(ListItem));
	li->this = a;
	li->next = NULL;
	return li;
}
List* newList(ListItem* start,ListItem* end)
{
	List* a = (List*)malloc(sizeof(List));
	a->start = start;
	a->end = end;
	return a;
}
void listInsert(List* l,ListItem* li)
{
	if(l->start == NULL)
	{
		l->end = l->start = li;
	}
	else
	{
		l->end->next = li;
		l->end = li;
	}
}
void listPrint(List* l)
{
	ListItem* curr = l->start;
	Node* _node;
	
	if(curr == NULL)
	{
		printf("\nNO RESULTS FOUND! \n");
		return;
	}
	else
	{
		printf("ROLL\t\tDATA\t\tHEIGHT IN AVL TREE\n");
		printf("--------------------------------------------------\n");
	}
	while(curr!=NULL)
	{
		_node = curr->this;
		printf("%d\t\t%s\t\t%d\n\n", _node->key,_node->data,_node->height+1);
		curr = curr->next;
	}
}
Node* insert(Node* _tree, Node* _n)
{
	Node* y = NULL;
	Node* x = _tree;
	while(x!=NULL)
	{
		y = x; // TRAVERSE THROUGH THE TREE UNTIL THE PROPER NODE IS FOUND
		if (x->key >= _n->key)
		{
			x = x->left;
		}
		else
		{
			x = x->right;
		}
	}
	_n->p = y;
	if(y == NULL)
	{
		_tree = _n; //THE TREE IS EMPTY
		//increaseHeight(_tree);
		return _tree;
	}
	//INSERT THE NODE IN THE PROPER SUBTREE

	if(y->key >= _n->key)
	{
		y->left = _n;
	}
	else
	{
		y->right = _n;
	}
	if(y->height == 0);

	int imBalanceFlag = 0;
	int canChangeHeight = 1;
	int tree_changed = 0;
	_n = y;
	Node *gp,*p,*c,*result;
	while(_n!=NULL)
	{
		result = _n;
		imBalanceFlag = getHImbalance(_n);
		//printf("imBalanceFlag: %d, Key: %d\n", imBalanceFlag,_n->key);		
		switch(imBalanceFlag)
		{
			case 0: //BALANCED
				//printf(" BALANCED\n");
			break;
			case -2: //RIGHT-LEFT
				//printf("Imbalance RIGHT-LEFT at %d\n", _n->key);
				gp = _n;
				p = _n->right;
				c = p->left;
				//Rotate 1
				p->left = c->right;
				if(c->right != NULL)
				{
					p->left->p = p;
				}					
				c->right = p;
				p->p = c;
				c->p=gp;
				gp->right = c;
				//Rotate 2
				gp->right = c->left;
				if(c->left != NULL)
					gp->right->p = gp;
				c->left = gp;
				c->p = gp->p;
				if(gp->p != NULL)
				{
					if(gp->p->left == gp)
					{
						gp->p->left = c;
					}
					else
					{
						gp->p->right = c;
					}	
				}
				else
				{
					_tree = c;
					tree_changed = 1;
				}
				gp->p = c;
				c->height++;
				p->height--;
				gp->height--;
			break;
			case -1: //RIGHT-RIGHT
				//printf("Imbalance RIGHT-RIGHT at %d\n", _n->key);
				gp = _n;
				p = _n->right;
				c = p->right;
				//Rotate 2
				gp->right = p->left;
				if(p->left != NULL)
					gp->right->p = gp;
				p->left = gp;
				p->p = gp->p;
				if(gp->p != NULL)
				{
					if(gp->p->left == gp)
					{
						gp->p->left = p;
					}
					else
					{
						gp->p->right = p;
					}	
				}
				else
				{
					_tree = p;
					tree_changed = 1;
				}
				gp->p = p;
				gp->height--;
			break;
			case 1: //LEFT-LEFT
				//printf("Imbalance LEFT-LEFT at %d\n", _n->key);
				gp = _n;
				p = _n->left;
				c = p->left;
				//Rotate 1
				gp->left = p->right;
				if(p->right != NULL)
					gp->left->p = gp;
				p->right = gp;
				p->p = gp->p;
				if(gp->p != NULL)
				{
					if(gp->p->left == gp)
					{
						gp->p->left = p;
					}
					else
					{
						gp->p->right = p;
					}	
				}
				else
				{
					_tree = p;
					tree_changed = 1;
				}
				gp->p = p;
				gp->height--;
			break;
			case 2: //LEFT-RIGHT
				//printf("Imbalance LEFT-RIGHT at %d\n", _n->key);
				gp = _n;
				p = _n->left;
				c = p->right;
				//Rotate 1
				p->right = c->left;
				if(c->left != NULL)
				{
					p->right->p = p;
				}
					
				c->left = p;
				p->p = c;
				c->p=gp;
				gp->left = c;
				//Rotate 2
				gp->left = c->right;
				if(c->right != NULL)
					gp->left->p = gp;
				c->right = gp;
				c->p = gp->p;
				if(gp->p != NULL)
				{
					if(gp->p->left == gp)
					{
						gp->p->left = c;
					}
					else
					{
						gp->p->right = c;
					}	
				}
				else
				{
					_tree = c;
					tree_changed = 1;
				}
				gp->p = c;
				c->height++;
				p->height--;
				gp->height--;
			break;
		}
		if(imBalanceFlag != 0)
		{
			canChangeHeight = 0;
			//printf(" At node %d Returning...\n",_n->key);
			//ROTATE
		}
		else if(canChangeHeight == 1)
		{
			int h;
			//printf("Changing Height of %d\n", _n->key);
			if(_n->left != NULL)
				h = _n->left->height;
			else
				h = -1;
			if(_n->right == NULL)
				_n->height = h+1;
			else if(_n->right != NULL)
				_n->height = h>_n->right->height?h+1:_n->right->height+1;
		}
		if(tree_changed == 1)
			break;
		_n = _n->p;
	}
	//printf("At node %d\n", _tree->key);
	return _tree;
} 
void search(Node* _tree,int k,List* result)
{
	if(result == NULL || _tree == NULL)
		return;
	int size = 0;
	if(_tree->key == k)
	{
		listInsert(result,newListItem(_tree));
		search(_tree->left,k,result);
		search(_tree->right,k,result);
	}
	else
	{
		if(k<_tree->key)
			search(_tree->left,k,result);
		else if(k>_tree->key)
			search(_tree->right,k,result);
	}			
}

Node* delete(Node* _tree,int key)
{
	List* result = newList(NULL,NULL);
	search(_tree,key,result);
	ListItem* curr = result->start;
	Node* toDelete;
	if(curr == NULL)
	{
		printf("Key doesn't exist.\n");
		return _tree;
	}
	Node *swap,*p,*temp,*c,*parent;

	while(curr!=NULL)
	{
		toDelete = curr->this;
		//free(curr);
		result->start = result->start->next;
		printf("\n");
		switch(countChild(toDelete))
		{
			case 0: // NO CHILD -- A LEAF
				// printf("Have no child\n");
				// printf("Deleting Key: %d Val: %s\n", toDelete->key,toDelete->data);
				if(toDelete->p != NULL)
				{
					if(toDelete == toDelete->p->right)
					{
						// printf("On the right of %d\n", toDelete->p->key);
						toDelete->p->right = NULL;
					}
					else
					{
						// printf("On the left of %d\n", toDelete->p->key);
						toDelete->p->left = NULL;
					}
					parent = toDelete->p;
					fixHeight(parent);
					_tree = fixImbalance(_tree,parent);
				}
				else
					_tree = NULL;
				//free(toDelete->data);
				free(toDelete);
				//free(curr);
			break;
			case 2: //HAVE BOTH CHILDREN
				// printf("Have both children\n");
			case -1: // HAVE ONLY RIGHT CHILD
				// printf("Have only right child\n");
				// printf("Deleting Key: %d Val: %s\n", toDelete->key,toDelete->data);
				//ioTraverse(_tree);
				swap = toDelete->right;
				while(swap->left!=NULL)
				{
					swap = swap->left;
				}
				// printf("Swapper: %d\n", swap->key);
				if(toDelete->p != NULL)
				{				
					if(toDelete == toDelete->p->left)
					{
						toDelete->p->left = swap;
					}
					else
					{
						toDelete->p->right = swap;
					}
				}
				else
				{
					// printf("Deleting Root node\n");
					_tree = swap;
				}
				swap->left = toDelete->left;
				
				if(toDelete->left!=NULL)
				{
					swap->left->p = swap;
				}

				

				if(swap->p != toDelete)
				{
					swap->p->left = swap->right;
					if(swap->right != NULL)
					{
						swap->right->p = swap->p;
						fixHeight(swap->p);
					}

					swap->right = toDelete->right;
					if(toDelete->right!=NULL)
					{
						swap->right->p = swap;
					}
					//swap->height = toDelete->height;
					
					Node* oldP = swap->p;
										
					swap->p = toDelete->p;
					temp = oldP;
					while(temp!=NULL)
					{
						fixHeight(temp);
						temp = temp->p;
					}
					_tree = fixImbalance(_tree,oldP);			
					free(toDelete);
				}
				else
				{
					// printf("Immediate Swapper!\n");
					swap->p = toDelete->p;
					
					temp = swap;
					while(temp!=NULL)
					{
						fixHeight(temp);
						temp = temp->p;
					}	
					_tree = fixImbalance(_tree,swap);			
					free(toDelete);
				}


			break;
			case 1: // HAVE ONLY LEFT CHILD
				// printf("Have only left child\n");
				// printf("Deleting Key: %d Val: %s\n", toDelete->key,toDelete->data);
				swap = toDelete->left;
				// printf("Swapper: %d\n", swap->key);
				if(toDelete->p != NULL)
				{				
					if(toDelete == toDelete->p->left)
					{
						toDelete->p->left = swap;
					}
					else
					{
						toDelete->p->right = swap;
					}
				}
				else
				{
					// printf("Deleting Root node\n");
					_tree = swap;
				}
				swap->p = toDelete->p;
				swap->height = 0;
				temp = swap;
				while(temp!=NULL)
				{
					fixHeight(temp);
					temp = temp->p;
				}
				_tree = fixImbalance(_tree,swap->p);
				free(toDelete);
			break;
		}
		curr = result->start;
	}
	return _tree;

}
int fixHeight(Node* node)
{
	if(node==NULL)
		return;
	int old = node->height;
	// printf("Fixing Height of %d\n", node->key);
	// printf("Old Height: %d, ", node->height);
	if(node->left == NULL && node->right == NULL)
		node->height=0; // NO CHILD -- A LEAF
	else if(node->right == NULL)
		node->height = node->left->height+1; // HAVE ONLY LEFT CHILD
	else if(node->left == NULL)
		node->height = node->right->height+1; // HAVE ONLY RIGHT CHILD
	else
		node->height = node->right->height > node->left->height?node->right->height+1:node->left->height+1; // HAVE BOTH CHILD
	// printf("New Height: %d\n", node->height);
	if(old != node->height)
		return 1; //HEIGHT HAS BEEN CHANGED(FIXED)
	return 0; //HEIGHT HASN'T BEEN CHANGED(NO FIX)
}
Node* rightRotate(Node* n)
{
	if(n == NULL)
		return n;
	Node* c = n->left;
	c->p = n->p;
	if(n->p != NULL)
	{
		if(n->p->left == n)
			n->p->left = c;
		else
			n->p->right = c;
	}
	n->left = c->right;
	c->right = n;
	
	if(n->left!=NULL)
		n->left->p = n;
	n->p = c;
	fixHeight(n);
	fixHeight(c);
	return c;
}
Node* leftRotate(Node* n)
{
	if(n==NULL)
		return n;
	Node* c = n->right;
	c->p = n->p;
	if(n->p != NULL)
	{
		if(n->p->left == n)
			n->p->left = c;
		else
			n->p->right = c;
	}
	n->right = c->left;
	c->left = n;
	
	if(n->right!=NULL)
		n->right->p = n;
	n->p = c;
	fixHeight(n);
	fixHeight(c);
	return c;
}

Node* fixImbalance(Node* _tree,Node* _n)
{
	if(_tree == NULL || _n==NULL)
		return;
	//printf("fix balance Called: %d\n", _n->key);
	int imBalanceFlag = 0;
	int canChangeHeight = 1;
	int tree_changed = 0;
	Node *gp,*p,*c,*result;
	while(_n!=NULL)
	{
		result = _n;
		imBalanceFlag = getHImbalance(_n);
		//printf("imBalanceFlag: %d, Key: %d\n", imBalanceFlag,_n->key);		
		switch(imBalanceFlag)
		{
			case 0: //BALANCED
				//printf(" BALANCED\n");
			break;
			case -2: //RIGHT-LEFT
				// printf("Imbalance RIGHT-LEFT at %d\n", _n->key);
				//Rotate 1
				_n->right = rightRotate(_n->right);
				//Rotate 2
				_n = leftRotate(_n);
			break;
			case -1: //RIGHT-RIGHT
				// printf("Imbalance RIGHT-RIGHT at %d\n", _n->key);
				//Rotate 2
				_n = leftRotate(_n);
			break;
			case 1: //LEFT-LEFT
				// printf("Imbalance LEFT-LEFT at %d\n", _n->key);
				//Rotate 1
				_n = rightRotate(_n);
			break;
			case 2: //LEFT-RIGHT
				// printf("Imbalance LEFT-RIGHT at %d\n", _n->key);
				//Rotate 1
				_n->left = leftRotate(_n->left);
				//Rotate 2
				_n = rightRotate(_n);
			break;
		}
		if(_n->p == NULL)
		{
			_tree = _n;
			tree_changed = 1;
		}
		_n = _n->p;
	}
	//printf("At node %d\n", _tree->key);
	return _tree;
}
int countChild(Node* _tree)
{
	if(_tree->left == NULL && _tree->right == NULL)
		return 0; // NO CHILD -- A LEAF
	if(_tree->right == NULL)
		return 1; // HAVE ONLY LEFT CHILD
	if(_tree->left == NULL)
		return -1; // HAVE ONLY RIGHT CHILD
	return 2; // HAVE BOTH CHILD
}
void ioTraverse(Node* _tree)
{
	if(_tree != NULL)
	{
		ioTraverse(_tree->left);
		printf("%d\t\t%s\t\t%d\n\n", _tree->key,_tree->data,_tree->height+1);
		// printf("\n");
		ioTraverse(_tree->right);
	}
}
void printTree(Node* _tree)
{
	if(_tree != NULL)
	{		
		printf("%d(H: %d)\n", _tree->key,_tree->height);
		if(_tree->left != NULL)
		{
			printf(" L: %d ", _tree->left->key);
		}
		if(_tree->right != NULL)
		{
			printf("R: %d", _tree->right->key);

		}
		printf("\n");
		printTree(_tree->left);
		
		// printf("\n");
		printTree(_tree->right);
	}
}
int getHImbalance(Node* _n)
{
	if(_n == NULL)
		return 0;
	int hDiff = 0;
	Node* parent;
	if(_n->left!= NULL && _n->right!= NULL)
	{
		hDiff = _n->left->height - _n->right->height;
		switch(hDiff)
		{
			case 0: //BALANCED
			case 1: //BALANCED
			case -1: //BALANCED
				hDiff = 0;
			break;
			case 2: //LEFT-LEFT(1) OR LEFT-RIGHT(2) CASE
				parent = _n->left;
				if(parent->left == NULL)
				{
					hDiff = 2;
				}
				else if(parent->right==NULL)
				{
					hDiff = 1;
				}
				else
				{
					hDiff = parent->left->height>parent->right->height?1:2;	
				}		
			break;
			case -2: //RIGTH-LEFT(-2) OR RIGHT-RIGHT(-1) CASE
				parent = _n->right;
				if(parent->right == NULL)
				{
					hDiff = -2;
				}
				else if(parent->left==NULL)
				{
					hDiff = -1;
				}
				else
				{
					hDiff = parent->left->height>parent->right->height?-2:-1;	
				}		
			break;
		}
	}
	else if(_n->left != NULL)
	{
		hDiff = _n->left->height + 1; //RIGHT BRANCH TO THE PARENT IS EMPTY
		if(hDiff>=2)
		{
			parent = _n->left;
			hDiff = parent->left!=NULL?1:2;
		}
		else
			hDiff = 0;
	}
	else if(_n->right != NULL)	
	{
		hDiff = _n->right->height + 1;
		if(hDiff>=2)
		{
			parent = _n->right;
			hDiff = parent->right!=NULL?-1:-2;
		}
		else
			hDiff = 0;
	}
	return hDiff;
}
