#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef double** Matrix ;
void print_matrix(Matrix matrix,int size);
Matrix strassens(Matrix a,Matrix b,int size);
Matrix sm(Matrix matrix,int size,int x,int y);
Matrix add(Matrix a,Matrix b,int size);
Matrix sub(Matrix a,Matrix b,int size);
Matrix get_Matrix(int n);
Matrix CreateMatrix(int size);
void Merge(int size,Matrix dest,Matrix a,Matrix b,Matrix c,Matrix d);
Matrix random_matrix_generator(int size);
void DeallocMatrix(Matrix a,int size);

long long int flops_count = 0,base_size =0;

int main()
{
	//FILE* file = fopen("stats.txt","a");
	time_t t;
	srand48((unsigned) time(&t));
	int n;
	printf("Please enter size:");
	scanf("%d",&n);
	printf("Please enter base size:");
	scanf("%d",&base_size);
	
	//fprintf(file,"Please enter size: %d\n",n);
	//fprintf(file,"Please enter base size: %lld",base_size);
	
	Matrix a,b;

	a = random_matrix_generator(n);
	b = random_matrix_generator(n);

	//printf("A:\n");
	//print_matrix(a,n);
	//printf("B:\n");
	//print_matrix(b,n);
	
	Matrix c = strassens(a,b,n);

	//printf("A*B:\n");
	//print_matrix(c,n);
	printf("\nTotal no of flops: %d\n",flops_count);
	//fprintf(file,"\nTotal no of flops: %lld\n",flops_count);
	//fclose(file);
	DeallocMatrix(a,n);
	DeallocMatrix(b,n);
	DeallocMatrix(c,n);
	return 0;
}
Matrix get_Matrix(int n)
{
	printf("Please enter a matrix:");
	int i,j;
	Matrix a = (Matrix)malloc(n*sizeof(double*));
	for( i=0;i<n;i++)
	{
	a[i] = (double*)malloc(n*sizeof(double));
	for( j=0;j<n;j++)
	{
	    //a[i][j] = i+j;
	    scanf("%lf",&a[i][j]);
	}
	}
	return a;
}
void DeallocMatrix(Matrix a,int size)
{
	int i;
	for(i=0;i<size;i++)
	{
		free(a[i]);
	}
	free(a);
}
void print_matrix(Matrix matrix,int size)
{
	int i,j;
	for(i=0;i<size;i++)
	{
		for(j=0;j<size;j++)
		{
		    printf("%2.2f ",matrix[i][j]);
		}
		printf("\n");
	}
}
Matrix SM(Matrix matrix,int size,int x,int y)
{
	int n = size/2;
	int i,j;
	Matrix submatrix;

	submatrix = (Matrix)malloc(n*sizeof(double*));
	for( i=0;i<n;i++)
	{

	submatrix[i] = &matrix[i+x*n][y*n];
	}

	return submatrix;
}
Matrix add(Matrix a,Matrix b,int size)
{
	Matrix c;
	int i,j;
	c = (Matrix)malloc(size*sizeof(double*));
	for(i=0;i<size;i++)
	{
		c[i] = (double*)malloc(size*sizeof(double));
		for( j=0;j<size;j++)
		{
		    c[i][j] = a[i][j]+b[i][j];flops_count++;
		}
	}
	return c;
}

Matrix sub(Matrix a,Matrix b,int size)
{
	Matrix c;
	int i,j;
	c = (Matrix)malloc(size*sizeof(double*));
	for( i=0;i<size;i++)
	{
		c[i] = (double*)malloc(size*sizeof(double));
		for( j=0;j<size;j++)
		{
		    c[i][j] = a[i][j]-b[i][j];flops_count++;
		}
	}
	return c;
}

Matrix strassens(Matrix a,Matrix b,int size)
{
	
	Matrix c ;
	int i,j,k;
	c = CreateMatrix(size);
	if(size <= base_size)
	{
		for( i=0;i<size;i++)
		{
		    for( j=0;j<size;j++)
		    {
			c[i][j] = a[i][0]*b[0][j]; flops_count++;
			for( k=1;k<size;k++)
			{
			    c[i][j] += a[i][k]*b[k][j];
			    flops_count+=2;
			}
		    }
		}
		return c;
	}
	Matrix p1,p2,p3,p4,p5,p6,p7;
	p1 = strassens(add(SM(a,size,0,0),SM(a,size,1,1),size/2),add(SM(b,size,0,0),SM(b,size,1,1),size/2),size/2);
	p2 = strassens(add(SM(a,size,1,0),SM(a,size,1,1),size/2),SM(b,size,0,0),size/2);
	p3 = strassens(SM(a,size,0,0),sub(SM(b,size,0,1),SM(b,size,1,1),size/2),size/2);
	p4 = strassens(SM(a,size,1,1),sub(SM(b,size,1,0),SM(b,size,0,0),size/2),size/2);
	p5 = strassens(add(SM(a,size,0,0),SM(a,size,0,1),size/2),SM(b,size,1,1),size/2);
	p6 = strassens(sub(SM(a,size,1,0),SM(a,size,0,0),size/2),add(SM(b,size,0,0),SM(b,size,0,1),size/2),size/2);
	p7 = strassens(sub(SM(a,size,0,1),SM(a,size,1,1),size/2),add(SM(b,size,1,0),SM(b,size,1,1),size/2),size/2);

	Merge(size,c,add(sub(add(p1,p4,size/2),p5,size/2),p7,size/2),add(p3,p5,size/2),add(p2,p4,size/2),add(sub(add(p1,p3,size/2),p2,size/2),p6,size/2));


	return c;
}
Matrix CreateMatrix(int size)
{
	int i,j;
	Matrix a = (Matrix)malloc(size*sizeof(double*));
	for( i=0;i<size;i++)
	{
		a[i] = (double*)malloc(size*sizeof(double));
	}
	return a;
}
void Merge(int size,Matrix dest,Matrix a,Matrix b,Matrix c,Matrix d)
{
	int i,j;	
	for( i=0;i<size/2;i++)
	{
		for(j=0;j<size/2;j++)
		{
		    dest[i][j] = a[i][j]*(i<size/2 && j<size/2);
		    dest[i][j + size/2] = b[i][j]*(i<size/2 && j<size/2);
		    dest[i + size/2][j] = c[i][j]*(i<size/2 && j<size/2);
		    dest[i + size/2][j + size/2] = d[i][j]*(i<size/2 && j<size/2);
		}
	}
}
Matrix random_matrix_generator(int size)
{
	int i,j;
	Matrix a = (Matrix)malloc(size*sizeof(double*));
	for( i=0;i<size;i++)
	{
		a[i] = (double*)malloc(size*sizeof(double));
		for( j=0;j<size;j++)
		{
		    a[i][j] = drand48()*10;
		}
	}
	return a;
	
}
