#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <limits.h>

#define LinearType 1
#define DoubleType 2
#define QuadType 3
#define ChainType 4
#define TraceMode 5

typedef int* Array;
typedef struct _primes{
	Array arr;
	int curr_size;
}Primes;
typedef struct _intNode{
	int key;
	struct _intNode* next;
}IntNode;

typedef struct _chain{
	IntNode** table;
	int height;
} Chain;
typedef struct _linHT{
	int* table;
	int size;
}HT;

int* get_array(int size);
void print_arr(int* a,int size);
HT create_lin_ht(int* a,int size,int init_size);
HT create_quad_ht(int* a,int size,int init_ht_size);
HT create_double_ht(int* a,int size,int init_size);
Array random_array(int size);
double load_factor(HT a);
void init_primes();
Chain create_chain(Array a,int size);
void print_chain(Chain chain);
IntNode* new_intnode(int val,IntNode* next);
int next_prime(int last);
int search(int key,HT a,int type);
int delete_key(int key,HT a,int type);
int insert(int key,HT* a,int type,double lf_target);
IntNode* search_chain(int key,Chain a);
void reset_probe_count();
int delete_key_chain(int key,Chain a);

void trace(double lf);

Primes* primes;
int MAX_LEN;

int collisions = 0;
int success_probe = 0;
int fail_probe = 0;
int rehash_cost = 0;
int flops = 0;

int main()
{
	int i;
	init_primes();
	printf("How many num do u have?\n");
	int size;
	scanf("%d",&MAX_LEN);
	//MAX_LEN = 12;
	time_t t;
	srand((unsigned) time(&t));
	size = MAX_LEN;
	int* a = random_array(size);
	//int* a = get_array(size);
	//int a[12] = {10,3,33,49,89,80,33,65,89,296,374,96};	
	printf("Main Array\n");
	print_arr(a,MAX_LEN);
	printf("----------------------------------\n");
	
	HT lin_ht = create_lin_ht(a,size,size);
	HT double_ht = create_double_ht(a,size,size);
	HT quad_ht = create_quad_ht(a,size,size);	
	Chain chain_ht = create_chain(a,size);

	int menu = -1;
	double target_lf = 0.2;

	while(1)
	{
		printf("\nPlease enter the type of Hashing: \n0. Exit\n1. Linear\n2. Double\n3. Quad\n4. Chained Linked List\n5. Enter Trace Mode\n");
		scanf("%d",&menu);
		if(menu == 0)
		{
			free(primes->arr);
			break;
		}
		switch(menu)
		{
			case LinearType:
				printf("*************Linear Probing***********\n");
				print_arr(lin_ht.table,lin_ht.size);
				printf("----------------------------------\n");
			break;
			case DoubleType:
				printf("***********Double Hashing*************\n");
				print_arr(double_ht.table,double_ht.size);
				printf("----------------------------------\n");
			break;
			case QuadType:
				printf("***********Quad Probing***************\n");
				print_arr(quad_ht.table,quad_ht.size);
				printf("----------------------------------\n");
			break;
			case ChainType:
				printf("***********Chained Hashing**************\n");
				print_chain(chain_ht);
				printf("----------------------------------\n");
			break;
			case TraceMode:
				printf("***********Trace Mode**************\n");
				for(target_lf = 0.25;target_lf<=0.75;target_lf+=0.1)
				{
					trace(target_lf);	
				}
				
			break;
		}

		int type,key,action;
		int search_status = 0;
		while(1)
		{
			if(menu == TraceMode)
			{
				break;
			}
			printf("\nPlease enter what you want to do: \n0. Last Menu\n1. Insert\n2. Search\n3. Delete\n");
			scanf("%d",&action);
			//printf("\nPlease enter where you want to search: \n0. Skip\n1. Linear\n2. Double\n3. Quad\n4. Chained Linked List\n");
			if(action == 0)
				break;
			printf("Please enter Key :\n");
			scanf("%d",&key);

			switch(action)
			{
				case 1: //Insert
				switch(menu)
				{
					case LinearType: // Linear
						insert(key,&lin_ht,LinearType,0.75);
						print_arr(lin_ht.table,lin_ht.size);
					break;
					case DoubleType: //Double
						insert(key,&double_ht,DoubleType,0.75);
						print_arr(double_ht.table,double_ht.size);
					break;
					case QuadType: //Quad
						insert(key,&quad_ht,QuadType,0.75);
						print_arr(quad_ht.table,quad_ht.size);
					break;
					case ChainType: //Chained linked list
						insert_chain(&chain_ht,key);
						print_chain(chain_ht);
						break;
					default:
						//printf("Wrong key entered...\n");
						break;

				}
				break;
				case 2: //Search
				switch(menu)
				{
					case LinearType: // Linear
						search_status = search(key,lin_ht,LinearType);
					break;
					case DoubleType: //Double
						search_status = search(key,double_ht,DoubleType);
					break;
					case QuadType: //Quad
						search_status = search(key,quad_ht,QuadType);
					break;
					case ChainType: //Chained linked list
						search_chain(key,chain_ht);
					break;
				}
				if(search_status == -1)
					printf("Key Not Found...\n");
				else
					printf("\nKey Found at index %d\n", search_status);

				break;
				case 3: //Delete
				switch(menu)
				{
					case LinearType: // Linear
						delete_key(key,lin_ht,LinearType);
					break;
					case DoubleType: //Double
						delete_key(key,double_ht,DoubleType);
					break;
					case QuadType: //Quad
						delete_key(key,quad_ht,QuadType);
					break;
					case ChainType: //Chained linked list
						delete_key_chain(key,chain_ht);
						print_chain(chain_ht);
						break;
					default:
						printf("Wrong key entered...\n");
						break;

				}
				break;
			}
				
		}


	}
	return 0;
}
void retrieve_stats()
{

}
void init_primes()
{
	int i;

	if(primes != NULL)
	{
		print_arr(primes->arr,primes->curr_size);
		printf("Freeing memory: %d\n",primes->curr_size);
		free(primes->arr);
		free(primes);		
	}
	primes = (Primes*)malloc(sizeof(Primes));
	primes->arr = (Array)malloc(1026*sizeof(int));
	primes->curr_size = 0;
	primes->arr[primes->curr_size++] = 2;
	primes->arr[primes->curr_size++] = 3;
	//print_arr(primes->arr,primes->curr_size);
}
int* get_array(int size)
{
	printf("Please enter a array:");
	int i;
	int* a = (int*)malloc(size*sizeof(int));
	for( i=0;i<size;i++)
	{
		scanf("%d",a+i);		
	}
	return a;
}
void print_arr(int* a,int size)
{
	int i;
	for( i=0;i<size;i++)
	{
		printf("Index: %d ",i);
		if(a[i] == INT_MIN || a[i] == INT_MAX)
			printf("~\n");
		else
			printf("%d\n",a[i]);
		
	}
	printf("\n");
}
HT create_lin_ht(int* a,int size,int init_size)
{
	rehash_cost = 0;
	HT result;
	int height = init_size<30?31:next_prime((init_size/2));
	double lf = 0.0;
	int* out = (int*)malloc(height*sizeof(int));
	int i,j;
	for(i=0;i<height;i++)
	{
		out[i] = INT_MIN;
	}
	
	int key;
	for (i = 0; i < size; ++i)
	{
		key = a[i];
		if(a[i] == INT_MIN || a[i] == INT_MAX)
			continue;

		for(j=0;j<height;j++)
		{
			if(a[i]==out[(a[i]+j)%height])
				break;
			flops++;rehash_cost++;
			if(out[(a[i]+j)%height] == INT_MIN)
			{
				out[(a[i]+j)%height] = a[i];
				lf += (1.0/height);
				//printf("a[%d] = %d stored at %d as %d\n",i,a[i],(a[i]+j)%MAX_LEN,out[(a[i]+j)%MAX_LEN]);
				break;
			}
						
		}
		if(lf>0.75)	
		{
			free(out);
			rehash_cost = 0;
			printf("Rehashing Now : %f at Size: %d\n", lf,height);
			result = create_lin_ht(a,size,2*init_size);
			printf("Rehashing Cost: %d\n",rehash_cost);
			return result;
		}

	}
	result.table = out;
	result.size = height;
	return result;
}
HT create_double_ht(int* a,int size,int init_size)
{
	HT result;
	int height = init_size<30?31:next_prime((init_size/2));
	double lf = 0.0;
	int* out = (int*)malloc(height*sizeof(int));
	int i,j;
	for(i=0;i<height;i++)
	{
		out[i] = INT_MIN;
	}	
	int key;
	for (i = 0; i < size; ++i)
	{
		key = a[i];
		if(a[i] == INT_MIN || a[i] == INT_MAX)
			continue;
		for(j=0;j<height;j++)
		{
			if(a[i]==out[(key+j*(7-key%7))%height])
				break;	
			if(out[(a[i]+j*(7-(a[i]%7)))%height] == INT_MIN)
			{
				out[(a[i]+j*(7-(a[i]%7)))%height] = a[i];
				lf += (1.0/height);
				//printf("a[%d] = %d stored at %d as %d\n",i,a[i],(a[i]+j)%height,out[(a[i]+j)%height]);
				break;
			}
			
		}	
		if(lf>0.75)	
		{
			printf("Rehashing Now : %f at Size: %d\n", lf,height);
			free(out);
			result = create_double_ht(a,size,2*init_size);
			return result;
		}	
	}
	//print_arr(out,MAX_LEN);
	//printf("Load Factor LPHT: %f\n",load_factor(out,size));
	
	result.table = out;
	result.size = height;
	return result;
}
HT create_quad_ht(int* a,int size,int init_size)
{
	int height = init_size<30?31:next_prime((init_size/2));
	HT result;
	result.size = height;
	int* out = (int*)malloc(height*sizeof(int));

	result.table = out;
	//printf("Height: %d\n",height);
	int i,j;
	for(i=0;i<height;i++)
	{
		out[i] = INT_MIN;
	}

	double lf = 0;
	int key;
	//printf("Load Factor 1: %f, Height: %d\n",lf,height);
	for (i = 0; i < size; ++i)
	{
		key = a[i];
		if(a[i] == INT_MIN || a[i] == INT_MAX)
			continue;
		for(j=0;j<height;j++)
		{
			if(a[i]==out[(key+j*j)%height])
				break;	
			if(out[(a[i]+j*j)%height] == INT_MIN)
			{
				out[(a[i]+j*j)%height] = a[i];
				lf = lf + (1.0/height);
				//printf("Load Factor inside: %f, Height: %d\n",lf,height);
				//printf("J = %d:a[%d] = %d stored at %d as %d\n",j,i,a[i],(a[i]+j*j)%height,out[(a[i]+j*j)%height]);
				break;
			}

		}
		if(lf>0.75 || j==height)
		{	
			printf("Load Factor QHT: %f, Rehashing now.\n",lf);		
			free(out);
			result = create_quad_ht(a,size,2*init_size);
			break;
		}		
	}
	//printf("Height: %d\n",height);
	//lf = load_factor(out,height);
	
	//print_arr(out,MAX_LEN);
	//printf("Load Factor QHT: %f\n",load_factor(out,size));
	
	
	printf("Load Factor : %f, Height: %d\n",lf,height);
	
	
	
	return result;
}
Chain create_chain(Array a,int size)
{
	int height = next_prime(15);
	Chain chain;
	chain.height = height;	
	chain.table = (IntNode**)malloc(height*sizeof(IntNode*));
	int i,j;
	for(i=0;i<height;i++)
	{
		chain.table[i] = new_intnode(0,NULL);	
		//printf("Added key in table[%d] %d\n", i,chain.table[i]->key);	
	}
	int repeatFlag=0;
	for (i = 0; i < size; ++i)
	{
		repeatFlag = 0;
		int loc = a[i]%height;
		IntNode* curr = NULL;
		curr = chain.table[loc];
		while(curr->next!=NULL)
		{
			if(curr->key == a[i])
			{
				repeatFlag = 1;
				break;
			}
			curr=curr->next;
		}
		if(repeatFlag == 0)
			curr->next = new_intnode(a[i],NULL);
	}
	return chain;	
}
IntNode* new_intnode(int val,IntNode* next)
{
	IntNode* a = (IntNode*)malloc(sizeof(IntNode));
	a->key = val;
	a->next = next;
	return a;
}
void print_chain(Chain chain)
{
	int h = chain.height,i;
	printf("Height of the chain: %d\n",chain.height);
	for (i = 0; i < h; ++i)
	{
		IntNode* curr = chain.table[i]->next;
		printf("\nIndex: %4d ",i);
		while(curr!=NULL)
		{
			if(curr->key == INT_MAX || curr->key == INT_MIN)
				printf("~, ");
			else
				printf("%d, ",curr->key);
			curr = curr->next;
		}
		
	}
}

Array random_array(int size)
{
	int i;
	Array a = (Array)malloc(size*sizeof(int));
	for( i=0;i<size;i++)
	{
		a[i] = rand()%10000;
	}
	return a;
	
}
double load_factor(HT a)
{
	double lf = 0;
	int i;
	Array table = a.table;
	for(i = 0 ;i <a.size;i++)
	{
		//printf("a[i] : %d > %d\n",a[i],INT_MIN);
		if(table[i]>INT_MIN && table[i] != INT_MAX)
		{
			lf++;
		}
	}
	lf /= a.size;
	return lf;
}
int search(int key,HT a,int type)
{
	int i = 0;
	int loc = key%a.size;
	int* table = a.table;
	int collision_counter = 0;
	switch(type)
	{
		case LinearType: // Linear
			
			while(i<a.size)
			{
				collision_counter++;
				if(table[(loc+i)%a.size] == key)
				{
					//printf("%d found at location %d\n", key,(loc+i)%a.size);
					success_probe+=collision_counter;
					return (loc+i)%a.size;
					break;
				}
				else if(table[(loc+i)%a.size] == INT_MIN)
				{
					//printf("Key is not in here.\n");
					break;
				}
				i++;
			}
		break;
		case DoubleType: //Double
			loc = key;
			while(i<a.size)
			{
				collision_counter++;
				if(table[(key+i*(7-(key%7)))%a.size] == key)
				{
					//printf("%d found at location %d\n", key,(key+i*(7-(key%7)))%a.size);
					success_probe+=collision_counter;
					return (key+i*(7-(key%7)))%a.size;
					break;
				}
				else if(table[(key+i*(7-(key%7)))%a.size] == INT_MIN)
				{
					//printf("Key is not in here.\n");
					break;
				}
				i++;
			}
		break;
		case QuadType: //Quad
			loc = key;
			while(i<a.size)
			{
				collision_counter++;
				if(table[(key+i*i)%a.size] == key)
				{
					//printf("%d found at location %d\n", key,(key+i*(7-(key%7)))%a.size);
					success_probe+=collision_counter;
					return (key+i*i)%a.size;
					break;
				}
				else if(table[(key+i*i)%a.size] == INT_MIN)
				{
					//printf("Key is not in here.\n");
					break;
				}
				i++;
			}
		break;
	}
	fail_probe += collision_counter;
	return -1;
}
int delete_key(int key,HT a,int type)
{
	int loc;
	switch(type)
	{
		case LinearType:
		case DoubleType:
		case QuadType:
			loc = search(key,a,type);
			a.table[loc] = INT_MAX;
		break;
	}
	print_arr(a.table,a.size);
}
int insert(int key,HT* a,int type,double lf_target)
{
	int j;
	double lf = load_factor(*a);
	HT* result;
	Array arr = a->table;
	int height = a->size;
	int rehash_flag = 0;
	switch(type)
	{
		case LinearType:			
			for(j=0;j<height;j++)
			{
				collisions++;
				if(key==a->table[(key+j)%height])
					break;	
				if(a->table[(key+j)%height] == INT_MIN || a->table[(key+j)%height] == INT_MAX)
				{
					a->table[(key+j)%height] = key;
					lf += (1.0/height);
					//printf("a[%d] = %d stored at %d as %d\n",i,a[i],(a[i]+j)%MAX_LEN,out[(a[i]+j)%MAX_LEN]);
					break;
				}
			}
			if(lf_target<lf)
			{
				rehash_flag = 1;
				*a = create_lin_ht(arr,a->size,2*a->size);
				
			}
		break;

		case DoubleType:
			for(j=0;j<height;j++)
			{
				collisions++;
				if(key==a->table[(key+j*(7-key%7))%height])
					break;	
				if(a->table[(key+j*(7-key%7))%height] == INT_MIN || a->table[(key+j*(7-key%7))%height] == INT_MAX)
				{
					a->table[(key+j*(7-key%7))%height] = key;
					lf += (1.0/height);
					//printf("a[%d] = %d stored at %d as %d\n",i,a[i],(a[i]+j)%MAX_LEN,out[(a[i]+j)%MAX_LEN]);
					break;
				}
			}
			if(lf_target<lf)
			{
				rehash_flag = 1;
				*a = create_double_ht(arr,a->size,2*a->size);
			}
		break;

		case QuadType:
			for(j=0;j<height;j++)
			{
				if(key==a->table[(key+j*j)%height])
					break;	
				collisions++;
				if(a->table[(key+j*j)%height] == INT_MIN || a->table[(key+j*j)%height] == INT_MAX)
				{
					a->table[(key+j*j)%height] = key;
					lf += (1.0/height);
					//printf("a[%d] = %d stored at %d as %d\n",i,a[i],(a[i]+j)%MAX_LEN,out[(a[i]+j)%MAX_LEN]);
					break;
				}
			}
			if(lf_target<lf)
			{
				rehash_flag = 1;
				*a = create_quad_ht(arr,a->size,2*a->size);
			}
		break;
	}
	if(rehash_flag == 1)
	{
		free(arr);
	}
	return 0;
}
int insert_chain(Chain* a,int key)
{
	int height = a->height;
	int loc = key%height;
	IntNode* curr = NULL;
	curr = a->table[loc];
	while(curr->next!=NULL)
	{
		if(curr->next->key == key)
		{
			return 0;
		}
		if(curr->key == INT_MAX)
		{
			curr->key = key;
			return 0;
		}
		curr=curr->next;
	}
	curr->next = new_intnode(key,NULL);
	return 0;
}
IntNode* search_chain(int key,Chain a)
{
	IntNode** table = a.table;
	int height = a.height;
	int row = key%height;
	IntNode* curr = table[row]->next;
	while(curr!=NULL)
	{
		if(curr->key == key)
		{
			printf("Key found in chain at index %d\n",row);
			return curr;
		}
		curr=curr->next;
	}
	return NULL;
}
int delete_key_chain(int key,Chain a)
{
	IntNode* loc_p = search_chain(key,a);
	loc_p->key = INT_MAX;
}
int next_prime(int last)
{	
	
	int limit = (2*last)%8192;
	Array arr = primes->arr;

	int curr_size = primes->curr_size;
	//printf("Current size %d\n", curr_size);
	int size_of_arr = 1026;
	int i = 0,guess,sq_root,hist;
	/**
	Search for the number 2*last first
	**/
	if(primes->arr[curr_size-1] > limit)
	{
		int mid = curr_size/2;
		int start = 0;
		int end = curr_size -1;
		if(arr[mid] > limit)
		{
			start = 0;
			end = mid;
		}
		else if(arr[mid] <limit)
		{
			start = mid;
			end = curr_size -1;
		}
		mid = (end + start)/2;
		while(end-start>1)
		{
			if(arr[mid] > limit)
			{
				end = mid;
			}
			else if(arr[mid] <limit)
			{
				start = mid;
			}
			mid = (end + start)/2;
		}
		guess = arr[end];
	}
	else
	{
		guess = arr[curr_size - 1];		
		while(guess<limit)
		{
			guess+=2;
			i = 0;
			sq_root = (int) sqrt(guess)+1;
			while(i<curr_size && arr[i]<=sq_root)
			{
				if(guess % arr[i] == 0)
				{
					guess ++;
					i = -1;
				}
				i++;
			}
			
			primes->arr[primes->curr_size++] = guess;
			curr_size++;
			if(guess > 10000)
				return;				
		}
	}
	//printf("Returning: %d\n",guess );
	return guess;
}
void trace(double lf)
{
	FILE* file = fopen("Result.txt","a");
	int size = 2000;
	int table_size = 2500;
	size = (int)((double)table_size*lf);	
	
	printf("\n**************STARTING TRACE**************\n");
	
	printf("\n\n");
	printf("Size of the input array: %f\n",table_size*lf);
	printf("\nTarget load factor: %lf\n",lf);
	
	fprintf(file,"Size of the input array: %f\n",table_size*lf);
	fprintf(file,"\nTarget load factor: %lf\n",lf);
	
	//printf("\nPlease Enter the size of table: \n");
	//scanf("%d",&size);
	printf("\n**************Linear Probe**************\n");
	
	fprintf(file,"\n**************Linear Probe**************\n");
	int i;
	
	int* a = random_array(size);
	
	int lin_probe_success = 0,lin_probe_fail=0;
	HT lin_ht = create_lin_ht(a,size,table_size);
	insert(1,&lin_ht,LinearType,lf);
	int* toSearch = random_array(size);
	int* toInsert = random_array(size);
	//toSearch = a;
	for (i = 0; i < size; ++i)
	{
		if(search(toSearch[i],lin_ht,LinearType)!=-1)
			lin_probe_success++;
		else
			lin_probe_fail++;
	}
	for (i = 0; i < size; ++i)
	{
		insert(toInsert[i],&lin_ht,LinearType,lf);
	}
	
	printf("\nStats: Linear Probing\n");
	printf("The size of the table: %d\n",lin_ht.size);
	printf("\nSuccess: %d, Failed: %d\n",lin_probe_success,lin_probe_fail);
	printf("Total collisions(search): %d\n", success_probe+fail_probe);
	printf("\nsuccess_probe: %f, fail_probe: %f\n", (double)success_probe/lin_probe_success,(double)fail_probe/lin_probe_fail);
	printf("\nCollisions: %d\n",collisions);
	
	fprintf(file,"\nStats: Linear Probing\n");	
	fprintf(file,"The size of the table: %d\n",lin_ht.size);
	fprintf(file,"\nLoad Factor: %f\n",load_factor(lin_ht));
	fprintf(file,"\nSuccess: %d, Failed: %d\n",lin_probe_success,lin_probe_fail);
	fprintf(file,"\nsuccess_probe: %f, fail_probe: %f\n", (double)success_probe/lin_probe_success,(double)fail_probe/lin_probe_fail);
	fprintf(file,"\nCollisions: %d\n",collisions);
	
	
	reset_probe_count();
	//free(a);
	free(lin_ht.table);
	//free(toSearch);
	//----------------------------------------------------------//

	printf("\n**************Quadratic Probe**************\n");
	
	fprintf(file,"\n**************Quadratic Probe**************\n");
	
	//a = random_array(size);
	
	
	int q_probe_success = 0,q_probe_fail=0;
	HT q_ht = create_quad_ht(a,size,table_size);
	insert(1,&q_ht,QuadType,lf);
	//toSearch = random_array(size);
	for (i = 0; i < size; ++i)
	{
		if(search(toSearch[i],q_ht,QuadType)!=-1)
			q_probe_success++;
		else
			q_probe_fail++;
	}
	for (i = 0; i < size; ++i)
	{
		insert(toInsert[i],&q_ht,QuadType,lf);
	}
	
	printf("\nStats: Quadratic Probing\n");
	printf("The size of the table: %d\n",q_ht.size);
	printf("\nSuccess: %d, Failed: %d\n",q_probe_success,q_probe_fail);
	printf("Total collisions(search): %d\n", success_probe+fail_probe);
	printf("\nsuccess_probe: %f, fail_probe: %f\n", (double)success_probe/q_probe_success,(double)fail_probe/q_probe_fail);
	printf("\nCollisions: %d\n",collisions);
	
	fprintf(file,"\nStats: Quadratic Probing\n");
	fprintf(file,"The size of the table: %d\n",q_ht.size);
	fprintf(file,"\nLoad Factor: %f\n",load_factor(q_ht));
	fprintf(file,"\nSuccess: %d, Failed: %d\n",q_probe_success,q_probe_fail);
	fprintf(file,"\nsuccess_probe: %f, fail_probe: %f\n", (double)success_probe/q_probe_success,(double)fail_probe/q_probe_fail);
	fprintf(file,"\nCollisions: %d\n",collisions);
	
	reset_probe_count();
	//free(a);
	free(q_ht.table);
	//free(toSearch);
	//----------------------------------------------------------//

	printf("\n**************Double Hashing**************\n");
	
	fprintf(file,"\n**************Double Hashing**************\n");
	
	//a = random_array(size);
	
	
	int d_probe_success = 0,d_probe_fail=0;
	HT d_ht = create_double_ht(a,size,table_size);
	insert(1,&d_ht,DoubleType,lf);
	//toSearch = random_array(size);
	for (i = 0; i < size; ++i)
	{
		if(search(toSearch[i],d_ht,DoubleType)!=-1)
			d_probe_success++;
		else
			d_probe_fail++;
	}
	for (i = 0; i < size; ++i)
	{
		insert(toInsert[i],&d_ht,DoubleType,lf);
	}
	printf("\nStats: Double Hashing\n");
	printf("The size of the table: %d\n",d_ht.size);
	printf("\nSuccess: %d, Failed: %d\n",d_probe_success,d_probe_fail);
	printf("Total collisions(search): %d\n", success_probe+fail_probe);
	printf("\nsuccess_probe: %f, fail_probe: %f\n", (double)success_probe/d_probe_success,(double)fail_probe/d_probe_fail);
	printf("\nCollisions: %d\n",collisions);
	
	fprintf(file,"\nStats: Double Hashing\n");
	fprintf(file,"The size of the table: %d\n",d_ht.size);
	fprintf(file,"\nLoad Factor: %f\n",load_factor(d_ht));
	fprintf(file,"\nSuccess: %d, Failed: %d\n",d_probe_success,d_probe_fail);
	fprintf(file,"\nsuccess_probe: %f, fail_probe: %f\n", (double)success_probe/d_probe_success,(double)fail_probe/d_probe_fail);
	fprintf(file,"\nCollisions: %d\n",collisions);
	
	reset_probe_count();
	free(a);
	free(d_ht.table);
	free(toSearch);
	free(toInsert);
	fclose(file);
}
void reset_probe_count()
{
	success_probe = 0;
	fail_probe = 0;
	collisions = 0;
}
